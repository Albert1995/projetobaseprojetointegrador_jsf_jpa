# TODO List #

* -> Configurar Projeto (DONE)
* -> Ajustar SRS -> Oracle DB para hsqldb; JSP para JSF (DONE)
* -> Ajustar models (DONE)
* -> Criar sql (DONE)
* -> Ajustar BCs (DONE)
* -> Criar DAO (DONE)
* -> Criar Telas xhtml
* -> Criar MB p telas

# JNDI #

Para acessar os serviços de acesso a base adicionem a seguinte linha:

    <Resource name="jdbc/repousadadb" auth="Container" type="javax.sql.DataSource"
               maxActive="50" maxIdle="30" maxWait="10000"
               username="root" password="root" 
               driverClassName="com.mysql.jdbc.Driver"
               url="jdbc:mysql://localhost:3306/repousada"/>

no Context.xml do TomCat.