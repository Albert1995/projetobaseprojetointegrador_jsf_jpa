SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- schema repousada
-- -----------------------------------------------------
DROP schema IF EXISTS `repousada` ;

-- -----------------------------------------------------
-- schema repousada
-- -----------------------------------------------------
CREATE schema IF NOT EXISTS `repousada` DEFAULT CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI ;
USE `repousada` ;

-- -----------------------------------------------------
-- TABLE `repousada`.`reserva`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`reserva` ;

CREATE TABLE IF NOT EXISTS `repousada`.`reserva` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `valor` DECIMAL NULL,
  `inicio` DATETIME NOT NULL,
  `fim` DATETIME NULL,
  PRIMARY KEY (`id`) )
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`nota_fiscal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`nota_fiscal` ;

CREATE TABLE IF NOT EXISTS `repousada`.`nota_fiscal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cnpj` VARCHAR(14) NOT NULL,
  `reserva_id` INT NOT NULL,
  `total` DECIMAL NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_nota_fiscal_reserva`
    FOREIGN KEY (`reserva_id`)
    REFERENCES `repousada`.`reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`pagamento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`pagamento` ;

CREATE TABLE IF NOT EXISTS `repousada`.`pagamento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `valor` DECIMAL NOT NULL,
  `dt_pagamento` DATETIME NOT NULL,
  `dt_prazo` DATETIME NOT NULL,
  `tp_pagamento` VARCHAR(45) NOT NULL,
  `nr_parcelas_restantes` INT,
  `reserva_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tipo_pagamento_reserva1`
    FOREIGN KEY (`reserva_id`)
    REFERENCES `repousada`.`reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`hospede`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`hospede` ;

CREATE TABLE IF NOT EXISTS `repousada`.`hospede` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(60) NOT NULL,
  `dt_nascimento` DATETIME NOT NULL,
  `cpf` VARCHAR(11) NOT NULL,
  `telefone` VARCHAR(14) NOT NULL,
  `cartao_credito` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`) )
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`funcionario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`funcionario` ;

CREATE TABLE IF NOT EXISTS `repousada`.`funcionario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(60) NOT NULL,
  `dt_nascimento` DATETIME NOT NULL,
  `cpf` VARCHAR(11) NOT NULL,
  `salario` DECIMAL NOT NULL,
  `telefone` VARCHAR(14) NOT NULL,
  `matricula` VARCHAR(20) NOT NULL UNIQUE,
  `senha` VARCHAR(45) NOT NULL,
  `tipo_funcionario` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`) )
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`hospede_reserva`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`hospede_reserva` ;

CREATE TABLE IF NOT EXISTS `repousada`.`hospede_reserva` (
  `hospede_id` INT NOT NULL,
  `reserva_id` INT NOT NULL,
  PRIMARY KEY (`hospede_id`, `reserva_id`),
  INDEX `fk_hospede_has_reserva_reserva1_idx` (`reserva_id` ASC),
  INDEX `fk_hospede_has_reserva_hospede1_idx` (`hospede_id` ASC),
  CONSTRAINT `fk_hospede_has_reserva_hospede1`
    FOREIGN KEY (`hospede_id`)
    REFERENCES `repousada`.`hospede` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hospede_has_reserva_reserva1`
    FOREIGN KEY (`reserva_id`)
    REFERENCES `repousada`.`reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;

-- -----------------------------------------------------
-- TABLE `repousada`.`servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`servico` ;

CREATE TABLE IF NOT EXISTS `repousada`.`servico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NOT NULL,
  `preco` DECIMAL NOT NULL,
  PRIMARY KEY (`id`) )
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`item_servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`item_servico` ;

CREATE TABLE IF NOT EXISTS `repousada`.`item_servico` (
  `servico_id` INT NOT NULL,
  `reserva_id` INT NOT NULL,
  `qtd` INT NOT NULL,
  PRIMARY KEY (`servico_id`, `reserva_id`),
  INDEX `fk_servico_has_reserva_reserva1_idx` (`reserva_id` ASC),
  INDEX `fk_servico_has_reserva_servico1_idx` (`servico_id` ASC),
  UNIQUE INDEX `UNIQUE` (`servico_id` ASC, `reserva_id` ASC),
  CONSTRAINT `fk_servico_has_reserva_servico1`
    FOREIGN KEY (`servico_id`)
    REFERENCES `repousada`.`servico` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servico_has_reserva_reserva1`
    FOREIGN KEY (`reserva_id`)
    REFERENCES `repousada`.`reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`quarto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`quarto` ;

CREATE TABLE IF NOT EXISTS `repousada`.`quarto` (
  `numero` INT NOT NULL,
  `tipo` VARCHAR(45) NOT NULL,
  `situacao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`numero`) )
ENGINE = INNODB;

-- -----------------------------------------------------
-- TABLE `repousada`.`diaria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`diaria` ;

CREATE TABLE IF NOT EXISTS `repousada`.`diaria` (
  `quarto_id` INT NOT NULL,
  `reserva_id` INT NOT NULL,
  `qtd` INT NOT NULL,
  `data` DATETIME NOT NULL,
  PRIMARY KEY (`quarto_id`, `reserva_id`),
  INDEX `fk_quarto_has_reserva_reserva1_idx` (`reserva_id` ASC),
  INDEX `fk_quarto_has_reserva_quarto1_idx` (`quarto_id` ASC),
  UNIQUE INDEX `UNIQUE` (`quarto_id` ASC, `reserva_id` ASC),
  CONSTRAINT `fk_quarto_has_reserva_quarto1`
    FOREIGN KEY (`quarto_id`)
    REFERENCES `repousada`.`quarto` (`numero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_quarto_has_reserva_reserva1`
    FOREIGN KEY (`reserva_id`)
    REFERENCES `repousada`.`reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`produto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`produto` ;

CREATE TABLE IF NOT EXISTS `repousada`.`produto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `preco` DECIMAL NOT NULL,
  PRIMARY KEY (`id`) )
ENGINE = INNODB;


-- -----------------------------------------------------
-- TABLE `repousada`.`item_produto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `repousada`.`item_produto` ;

CREATE TABLE IF NOT EXISTS `repousada`.`item_produto` (
  `reserva_id` INT NOT NULL,
  `produto_id` INT NOT NULL,
  `qtd` INT NOT NULL,
  PRIMARY KEY (`reserva_id`, `produto_id`),
  INDEX `fk_reserva_has_produto_produto1_idx` (`produto_id` ASC),
  INDEX `fk_reserva_has_produto_reserva1_idx` (`reserva_id` ASC),
  UNIQUE INDEX `UNIQUE` (`reserva_id` ASC, `produto_id` ASC),
  CONSTRAINT `fk_reserva_has_produto_reserva1`
    FOREIGN KEY (`reserva_id`)
    REFERENCES `repousada`.`reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reserva_has_produto_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `repousada`.`produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = INNODB;

INSERT INTO repousada.funcionario VALUES (null, 'Admin', now(), '01234567891', 12000.0, '88881234', 'admin', 'admin', 'ADMINISTRADOR');

INSERT INTO repousada.servico VALUES(null, 'Night Club', 35.00);
INSERT INTO repousada.servico VALUES(null, 'Cabelereiro', 25.00);
INSERT INTO repousada.servico VALUES(null, 'Personal Trainer', 85.00);

INSERT INTO repousada.produto VALUES(null, 'Champagne', 78.00);
INSERT INTO repousada.produto VALUES(null, 'Roupa de Banho', 33.00);
INSERT INTO repousada.produto VALUES(null, 'Pizza', 58.00);

INSERT INTO repousada.reserva VALUES(null, null, now(), null);

INSERT INTO repousada.item_servico VALUES(1, 1, 3);
INSERT INTO repousada.item_servico VALUES(2, 1, 1);

INSERT INTO repousada.item_produto VALUES(1, 2, 3);
INSERT INTO repousada.item_produto VALUES(1, 3, 8);

INSERT INTO repousada.quarto VALUES(1, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(2, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(3, "Simples" , "Ocupado");
INSERT INTO repousada.quarto VALUES(4, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(5, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(6, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(7, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(8, "Simples" , "Ocupado");
INSERT INTO repousada.quarto VALUES(9, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(10, "Simples" , "Disponivel");
INSERT INTO repousada.quarto VALUES(11, "Duplo" , "Disponivel");
INSERT INTO repousada.quarto VALUES(12, "Duplo" , "Disponivel");
INSERT INTO repousada.quarto VALUES(13, "Duplo" , "Reservado");
INSERT INTO repousada.quarto VALUES(14, "Duplo" , "Disponivel");
INSERT INTO repousada.quarto VALUES(15, "Duplo" , "Disponivel");
INSERT INTO repousada.quarto VALUES(16, "Duplo" , "Ocupado");
INSERT INTO repousada.quarto VALUES(17, "Duplo" , "Disponivel");
INSERT INTO repousada.quarto VALUES(18, "Duplo" , "Disponivel");
INSERT INTO repousada.quarto VALUES(19, "Duplo" , "Disponivel");
INSERT INTO repousada.quarto VALUES(20, "Duplo" , "Manutencao");
INSERT INTO repousada.quarto VALUES(21, "Casal" , "Disponivel");
INSERT INTO repousada.quarto VALUES(22, "Casal" , "Ocupado");
INSERT INTO repousada.quarto VALUES(23, "Casal" , "Disponivel");
INSERT INTO repousada.quarto VALUES(24, "Casal" , "Disponivel");
INSERT INTO repousada.quarto VALUES(25, "Casal" , "Disponivel");
INSERT INTO repousada.quarto VALUES(26, "Familia" , "Disponivel");
INSERT INTO repousada.quarto VALUES(27, "Familia" , "Ocupado");
INSERT INTO repousada.quarto VALUES(28, "Familia" , "Disponivel");
INSERT INTO repousada.quarto VALUES(29, "Familia" , "Disponivel");
INSERT INTO repousada.quarto VALUES(30, "Familia" , "Disponivel");

INSERT INTO repousada.hospede VALUES(null, "Maria",'1994-7-04' ,"12345678901", "(41)3245-3570", "354363");
INSERT INTO repousada.hospede VALUES(null, "Lucia",'1979-2-28' ,"85432148690", "(41)9457-3578", "3534646");


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
