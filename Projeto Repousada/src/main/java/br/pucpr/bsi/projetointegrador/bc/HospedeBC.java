package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import br.pucpr.bsi.projetointegrador.dao.HospedeDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Hospede;

public class HospedeBC extends PessoaBC<Hospede>{
	
	private static HospedeBC instance = new HospedeBC();
	
	private HospedeDAO dao = new HospedeDAO();
	
	private HospedeBC() {}
	
	public static HospedeBC getInstance()
	{
		return instance;
	}

	@Override
	public Long create(Hospede object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Hospede retrieve(Hospede object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Hospede> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Hospede object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(Hospede object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		dao.delete(object);
	}
	
	@Override
	protected void validate(Hospede object)
	{
		super.validate(object);
	}

	@Override
	public List<Hospede> retrievaByFilter(Hospede object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	protected void validateFilter(Hospede object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}
	
}
