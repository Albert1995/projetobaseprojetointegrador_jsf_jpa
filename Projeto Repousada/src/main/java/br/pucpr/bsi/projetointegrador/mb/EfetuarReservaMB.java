package br.pucpr.bsi.projetointegrador.mb;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.pucpr.bsi.projetointegrador.bc.HospedeBC;
import br.pucpr.bsi.projetointegrador.bc.QuartoBC;
import br.pucpr.bsi.projetointegrador.bc.ReservaBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.mb.messages.MessagesUtils;
import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;

@ManagedBean
@ViewScoped
public class EfetuarReservaMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Quarto quartoSelecionado;
	public Reserva reserva;
	public List<Hospede> hospedes;
	public List<Quarto> quartos;
	public Date today;
	public Date nextDate;

	public EfetuarReservaMB() {}

	@PostConstruct
	public void init() {
		hospedes = HospedeBC.getInstance().retrievaAll();
		Quarto q = new Quarto(null);
		q.setSituacao(SituacaoQuarto.DISPONIVEL);
		quartos = QuartoBC.getInstance().retrievaByFilter(q);
		reserva = new Reserva();
		today = new Date();
	}

	public void reservar() 
	{
		if(reserva.getFim() != null && reserva.getInicio() != null){
			long diffDays = (reserva.getFim().getTime() - reserva.getInicio().getTime()) / (24 * 60 * 60 * 1000);
			Calendar c = Calendar.getInstance();
			c.setTime(reserva.getInicio());
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);

			Calendar today = Calendar.getInstance();
			today.setTime(new Date());
			today.set(Calendar.HOUR_OF_DAY, 0);
			today.set(Calendar.MINUTE, 0);
			today.set(Calendar.SECOND, 0);
			today.set(Calendar.MILLISECOND, 0);

			if(quartoSelecionado != null){
				if(c.compareTo(today) == 0)
					quartoSelecionado.setSituacao(SituacaoQuarto.OCUPADO);
				else
					quartoSelecionado.setSituacao(SituacaoQuarto.RESERVADO);


				for (int i = 0; i < diffDays; i++) {
					reserva.adicionarDiaria(new Diaria(quartoSelecionado, c.getTime(), 1L, reserva));
					c.add(Calendar.DAY_OF_YEAR, 1);
				}
				QuartoBC.getInstance().update(quartoSelecionado);
			}else{
				throw new RepousadaException("É preciso selecionar um quarto");
			}
			ReservaBC.getInstance().create(reserva);
		}else{
			throw new RepousadaException("As datas não podem ser nulas");
		}

		MessagesUtils.addInfo("", "Reserva efetuada com sucesso!");
	}

	public Quarto getQuartoSelecionado() {
		return quartoSelecionado;
	}

	public void setQuartoSelecionado(Quarto quartoSelecionado) {
		this.quartoSelecionado = quartoSelecionado;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public List<Hospede> getHospedes() {
		return hospedes;
	}

	public void setHospedes(List<Hospede> hospedes) {
		this.hospedes = hospedes;
	}

	public List<Quarto> getQuartos() {
		return quartos;
	}

	public void setQuartos(List<Quarto> quartos) {
		this.quartos = quartos;
	}

	public Date getToday() {
		return today;
	}

	public void setToday(Date today) {
		this.today = today;
	}

	public Date getNextDate() {
		if(reserva.getInicio() != null){
			Calendar c = Calendar.getInstance();
			c.setTime(reserva.getInicio());
			c.add(Calendar.DATE, 1);
			nextDate = c.getTime();
			return nextDate;
		}
		return new Date();

	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

}
