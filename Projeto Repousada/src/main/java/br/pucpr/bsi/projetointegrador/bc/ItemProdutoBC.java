package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import br.pucpr.bsi.projetointegrador.dao.ItemProdutoDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.ItemProduto;

public class ItemProdutoBC extends PatternBC<ItemProduto>{

	
	private static final ItemProdutoBC instance = new ItemProdutoBC();
	
	private ItemProdutoDAO dao = new ItemProdutoDAO();
	
	private ItemProdutoBC() {}
	
	public static ItemProdutoBC getInstance() {
		return instance;
	}
	
	@Override
	public Long create(ItemProduto object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public ItemProduto retrieve(ItemProduto object) {
		return dao.retrieveById(object);
	}

	@Override
	public List<ItemProduto> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public List<ItemProduto> retrievaByFilter(ItemProduto object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	public void update(ItemProduto object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(ItemProduto object) {
		dao.delete(object);
	}

	@Override
	protected void validate(ItemProduto object) {
		if(object == null)
			throw new RepousadaException("Item de produto nulo");
		else if (object.getReserva() == null || object.getReserva().getId() == null)
			throw new RepousadaException("A solicita��o de produto deve estar vinculada a uma reserva");
		else if (object.getQuantidade() == null || object.getQuantidade() <= 0)
			throw new RepousadaException("Quantidade de item produto n�o informada ou menor ou igual a zero");
		else if (object.getProduto() == null || object.getProduto().getId() == null)
			throw new RepousadaException("Produto n�o informado");
		
	}

	@Override
	protected void validateFilter(ItemProduto object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}

}
