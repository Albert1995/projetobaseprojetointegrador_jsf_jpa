package br.pucpr.bsi.projetointegrador.models.enums;

import org.apache.commons.lang3.StringUtils;

public enum TipoPagamento {
	CREDITO(1l, "CREDITO"),
	DEBITO(2l, "DEBITO"), 
	DINHEIRO(3l, "DINHEIRO"),
	CHEQUE(4l, "CHEQUE");
	
	private Long id;
	private String nome;
	
	private TipoPagamento(Long id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public static TipoPagamento getTipoPagamento(String nome) {
		if(StringUtils.isBlank(nome))
			return null;
			
		for (TipoPagamento tipoPagamento : TipoPagamento.values()) {
			if(tipoPagamento.getNome().equals(nome))
				return tipoPagamento;
		}
		
		return null;
	}
}
