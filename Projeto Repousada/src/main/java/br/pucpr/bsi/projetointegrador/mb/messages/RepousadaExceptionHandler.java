package br.pucpr.bsi.projetointegrador.mb.messages;

import java.util.Iterator;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;

public class RepousadaExceptionHandler extends ExceptionHandlerWrapper {
    private static final Logger log = Logger.getLogger(RepousadaExceptionHandler.class.getCanonicalName());
    private ExceptionHandler wrapped;
 
    RepousadaExceptionHandler(ExceptionHandler exception) {
        this.wrapped = exception;
    }
 
    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }
 
    @Override
    public void handle() throws FacesException {
 
        final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context =
                    (ExceptionQueuedEventContext) event.getSource();
 
            // get the exception from context
            Throwable t = context.getException();
 
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, Object> requestMap = fc.getExternalContext().getRequestMap();
            final NavigationHandler nav = fc.getApplication().getNavigationHandler();
 
            //here you do what ever you want with exception
            try {
 
                //log error ?
                log.log(Level.ERROR, "Critical Exception!", t);
 
                //redirect error page
                requestMap.put("exceptionMessage", t.getMessage());
                
                if(t.getMessage().contains("RepousadaException")){
                	nav.handleNavigation(fc, null, null);	
                    String message = t.getMessage().substring(t.getMessage().lastIndexOf(":")+2);
                    MessagesUtils.addError("Erro", message);
                } else if(t.getCause() instanceof RepousadaException){
                	nav.handleNavigation(fc, null, null);	
                    MessagesUtils.addError("Erro", t.getMessage());
                } else {
                	nav.handleNavigation(fc, null, "error");
                    MessagesUtils.addError("Erro", "Erro interno do sistema");
                }
                
                fc.renderResponse();
                
            } finally {
                //remove it from queue
                i.remove();
            }
        }
        //parent hanle
        getWrapped().handle();
    }
}
