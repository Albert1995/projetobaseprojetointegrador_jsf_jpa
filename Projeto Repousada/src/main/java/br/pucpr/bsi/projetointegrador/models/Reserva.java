package br.pucpr.bsi.projetointegrador.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Reserva implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -760256839140674630L;

	private Long id;
	private List<Diaria> diarias;
	private List<Hospede> hospedes;
	private List<NotaFiscal> notaFiscal;
	private List<ItemServico> itemServicos;
	private List<ItemProduto> itemProdutos;
	private List<Pagamento> pagamentos;
	private Date inicio;
	private Date fim;
	private Double precoFinal;
	
	public Reserva() {}
	
	public Reserva(Long id) {
		this.id = id;
	}

	public Reserva(List<Hospede> hospedes) {
		this.precoFinal = new Double(0);
		this.hospedes = hospedes;
	}
	
	public Reserva(Hospede hospede){
		this.precoFinal = new Double(0);
		getHospedes().add(hospede);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Diaria> getDiarias() {
		return (diarias == null) ? diarias = new ArrayList<>() : diarias;
	}

	public List<Hospede> getHospedes() {
		return (hospedes == null) ? hospedes = new ArrayList<>() : hospedes;
	}

	public List<NotaFiscal> getNotaFiscal() {
		return (notaFiscal == null) ? notaFiscal = new ArrayList<>() : notaFiscal;
	}

	public List<ItemServico> getItemServicos() {
		return (itemServicos == null) ? itemServicos = new ArrayList<>() : itemServicos;
	}

	public List<Pagamento> getPagamentos() {
		return (pagamentos == null) ? pagamentos = new ArrayList<>() : pagamentos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ItemProduto> getItemProdutos() {
		return (itemProdutos == null) ? itemProdutos = new ArrayList<>() : itemProdutos;
	}

	public void adicionarDiaria(Diaria diaria) {
		this.getDiarias().add(diaria);
	}

	public void adicionarHospede(Hospede hospede) {
		this.getHospedes().add(hospede);
	}
	
	public Double calcularReserva(){
		precoFinal = getValorTotalDiarias() + getValorTotalProdutos() + getValorTotalServicos();
		return precoFinal;
	}
	
	public void setValorFinal(Double valor) {
		precoFinal = valor;
	}
	
	public Double getValorFinal() {
		return precoFinal;
	}

	/**
	 * Método para adicionar um novo consumo do hóspede. Esse consumo pode ser um produto ou serviço.
	 * 
	 * @param consumo
	 */
	public void adicionarConsumo(Object consumo, Long qtde){
		boolean temConsumo = false;

		if(consumo != null)
		{
			if(consumo instanceof Servico){
				//verifica se o servi�o foi solicitado antes e, em caso positivo, atualiza a quantidade em ItemServico
				temConsumo = verificarServicoExistente((Servico)consumo);

				//caso o servi�o n�o tenha sido solicitado, cria um novo ItemServico e adiciona na lista
				if(!temConsumo){
					ItemServico item = new ItemServico((Servico) consumo, qtde, this);
					itemServicos.add(item);
				}
			}else if(consumo instanceof Produto){
				//verifica se o produto foi consumido antes e, em caso positivo, atualiza a quantidade em ItemProduto
				temConsumo = verificarProdutoExistente((Produto)consumo);

				//caso o produto n�o tenha sido consumido, cria um novo ItemProduto e adiciona na lista
				if(!temConsumo){
					ItemProduto item = new ItemProduto((Produto) consumo, qtde, this);
					itemProdutos.add(item);
				}

			}
		}
	}

	/**
	 * M�todo utilit�rio para verificar se o servi�o que foi solicitado j�
	 * existe na lista de servi�os. Caso exista, a quantidade de ItemServico
	 * apenas ser� atualizada
	 * 
	 * @param consumo
	 * @return
	 */
	public boolean verificarServicoExistente(Servico consumo){

		if(!itemServicos.isEmpty()){
			for(ItemServico item : itemServicos){
				if(item.getServico().getTipo().equals( consumo.getTipo())){
					item.setQuantidade(item.getQuantidade() + 1);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * M�todo utilit�rio para verificar se o produto que foi consumido j�
	 * existe na lista de produtos. Caso exista, a quantidade de ItemProduto
	 * apenas ser� atualizada
	 * 
	 * @param consumo
	 * @return
	 */
	public boolean verificarProdutoExistente(Produto consumo){

		if(!itemProdutos.isEmpty()){
			for(ItemProduto item : itemProdutos){
				if(item.getProduto().getNome().equals((consumo.getNome()))){
					item.setQuantidade(item.getQuantidade() + 1);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * M�todo utilit�rio para calcular o valor total das di�rias
	 * @return
	 */
	public Double getValorTotalDiarias(){
		Double total = new Double(0.0);

		for (Diaria diaria : getDiarias()) 
			total += (diaria.getQuarto().getModelo().getPreco() * diaria.getQuantidade());

		return total;
	}
	
	
	/**
	 * M�todo utilit�rio para calcular o valor total dos produtos consumidos
	 * @return
	 */
	public Double getValorTotalProdutos(){
		Double total = new Double(0.0);

		if(itemProdutos != null && !itemProdutos.isEmpty()){
			for (ItemProduto p : itemProdutos) 
				total += (p.getProduto().getPreco() * p.getQuantidade());
		}

		return total;
	}
	
	/**
	 * M�todo utilit�rio para calcular o valor total dos servi�os utilizados
	 * @return
	 */
	public Double getValorTotalServicos(){
		Double total = new Double(0.0);

		if(itemServicos != null && !itemServicos.isEmpty()){
			for (ItemServico s: itemServicos) 
				total += (s.getServico().getPreco() * s.getQuantidade());
		}

		return total;
	}
	
	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reserva other = (Reserva) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setHospedes(List<Hospede> hospedes) {
		this.hospedes = hospedes;
	}

}
