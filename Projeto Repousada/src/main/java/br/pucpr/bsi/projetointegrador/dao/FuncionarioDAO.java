package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Funcionario;
import br.pucpr.bsi.projetointegrador.models.enums.TipoFuncionario;

public final class FuncionarioDAO implements DataAccessObject<Funcionario> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.funcionario";
	String retrieve = "SELECT id, nome, dt_nascimento, cpf, salario, telefone, matricula, tipo_funcionario FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(Funcionario element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable 
				+ " (nome, dt_nascimento, cpf, salario, telefone, matricula, senha, tipo_funcionario)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
				
				pstm.setString(1, element.getNome());
				pstm.setDate(2, new Date(element.getDataNascimento().getTime()));
				pstm.setString(3, element.getCpf());
				pstm.setDouble(4, element.getSalario());
				pstm.setString(5, element.getTelefone());
				pstm.setString(6, element.getMatricula());
				pstm.setString(7, element.getSenha());
				pstm.setString(8, element.getTipoFuncionario().name());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
				try (ResultSet generatedKeys = pstm.getGeneratedKeys()) {
		            if (generatedKeys.next()) {
		            	id = generatedKeys.getLong(1);
		            }  else {
		                throw new RepousadaException("INSERTING ELEMENT FAILED, NO ID OBTAINED.");
		            }
		        }
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(Funcionario element) {
		
		String update = "UPDATE " + dbtable 
				+ " SET nome = ?, dt_nascimento = ?, cpf = ?, salario = ?, "
				+ "telefone = ?, matricula = ?, senha = ? tipo_funcionario = ? WHERE id = ?";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setString(1, element.getNome());
				pstm.setDate(2, new Date(element.getDataNascimento().getTime()));
				pstm.setString(3, element.getCpf());
				pstm.setDouble(4, element.getSalario());
				pstm.setString(5, element.getTelefone());
				pstm.setString(6, element.getMatricula());
				pstm.setString(7, element.getSenha());
				pstm.setString(8, element.getTipoFuncionario().name());
				pstm.setLong(9, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(Funcionario element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<Funcionario> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<Funcionario> funcionarios = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				funcionarios = new ArrayList<>();
				
				while(rs.next()) {
					Funcionario funcionario = new Funcionario();
					funcionario.setId(rs.getLong("id"));
					funcionario.setNome(rs.getString("nome"));

					Date dt_nasc = rs.getDate("dt_nascimento");
					funcionario.setDataNascimento(dt_nasc == null? null : new java.util.Date(dt_nasc.getTime()));

					funcionario.setNome(rs.getString("cpf"));
					funcionario.setCpf(rs.getString("telefone"));
					funcionario.setSalario(rs.getDouble("salario"));
					funcionario.setTelefone(rs.getString("telefone"));
					funcionario.setMatricula(rs.getString("matricula"));
					funcionario.setTipoFuncionario(TipoFuncionario.getTipoFuncionario(rs.getString("tipo_funcionario")));
					
					funcionarios.add(funcionario);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return funcionarios;
	}

	@Override
	public Funcionario retrieveById(Funcionario element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getId());
		
		Funcionario funcionario = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					funcionario = new Funcionario();
					funcionario.setId(rs.getLong("id"));
					funcionario.setNome(rs.getString("nome"));

					Date dt_nasc = rs.getDate("dt_nascimento");
					funcionario.setDataNascimento(dt_nasc == null? null : new java.util.Date(dt_nasc.getTime()));

					funcionario.setNome(rs.getString("cpf"));
					funcionario.setCpf(rs.getString("telefone"));
					funcionario.setSalario(rs.getDouble("salario"));
					funcionario.setTelefone(rs.getString("telefone"));
					funcionario.setMatricula(rs.getString("matricula"));
					funcionario.setTipoFuncionario(TipoFuncionario.getTipoFuncionario(rs.getString("tipo_funcionario")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return funcionario;
	}

	@Override
	public List<Funcionario> retrieveByFilter(Funcionario element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<Funcionario> funcionarios = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getId() != null)
				retrieveByFilter.append(" AND id = ?");
			
			if(StringUtils.isNotBlank(element.getNome()))
				retrieveByFilter.append(" AND nome = ?");
			
			if(element.getDataNascimento() != null)
				retrieveByFilter.append(" AND dt_nascimento = ?");
			
			if(StringUtils.isNotBlank(element.getCpf()))
				retrieveByFilter.append(" AND cpf = ?");
			
			if(element.getSalario() != null)
				retrieveByFilter.append(" AND salario = ?");
			
			if(StringUtils.isNotBlank(element.getTelefone()))
				retrieveByFilter.append(" AND telefone = ?");

			if(StringUtils.isNotBlank(element.getMatricula()))
				retrieveByFilter.append(" AND matricula = ?");
			
			if(StringUtils.isNotBlank(element.getTipoFuncionario().getNome()))
				retrieveByFilter.append(" AND tipo_funcionario = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getId() != null)
					pstm.setLong(i++, element.getId());
				
				if(StringUtils.isNotBlank(element.getNome()))
					pstm.setString(i++, element.getNome());
					
				if(element.getDataNascimento() != null)
					pstm.setDate(i++, new Date(element.getDataNascimento().getTime()));
				
				if(StringUtils.isNotBlank(element.getCpf()))
					pstm.setString(i++, element.getCpf());
				
				if(element.getSalario() != null)
					pstm.setDouble(i++, element.getSalario());
				
				if(StringUtils.isNotBlank(element.getTelefone()))
					pstm.setString(i++, element.getTelefone());
				
				if(StringUtils.isNotBlank(element.getMatricula()))
					pstm.setString(i++, element.getMatricula());
				
				if(StringUtils.isNotBlank(element.getTipoFuncionario().getNome()))
					pstm.setString(i++, element.getTelefone());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				funcionarios = new ArrayList<>();
				
				while(rs.next()) {
					Funcionario funcionario = new Funcionario();
					funcionario.setId(rs.getLong("id"));
					funcionario.setNome(rs.getString("nome"));

					Date dt_nasc = rs.getDate("dt_nascimento");
					funcionario.setDataNascimento(dt_nasc == null? null : new java.util.Date(dt_nasc.getTime()));

					funcionario.setNome(rs.getString("cpf"));
					funcionario.setCpf(rs.getString("telefone"));
					funcionario.setSalario(rs.getDouble("salario"));
					funcionario.setTelefone(rs.getString("telefone"));
					funcionario.setMatricula(rs.getString("matricula"));
					funcionario.setTipoFuncionario(TipoFuncionario.getTipoFuncionario(rs.getString("tipo_funcionario")));
					funcionarios.add(funcionario);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return funcionarios;
		
	}

	public Funcionario autenticar(String matricula, String senha) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY MATRICULA: " + matricula + " AND PWD");
		
		Funcionario funcionario = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE matricula = ? AND senha = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setString(1, matricula);
				pstm.setString(2, senha);
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					funcionario = new Funcionario();
					funcionario.setId(rs.getLong("id"));
					funcionario.setNome(rs.getString("nome"));

					Date dt_nasc = rs.getDate("dt_nascimento");
					funcionario.setDataNascimento(dt_nasc == null? null : new java.util.Date(dt_nasc.getTime()));

					funcionario.setNome(rs.getString("cpf"));
					funcionario.setCpf(rs.getString("telefone"));
					funcionario.setSalario(rs.getDouble("salario"));
					funcionario.setTelefone(rs.getString("telefone"));
					funcionario.setMatricula(rs.getString("matricula"));
					funcionario.setTipoFuncionario(TipoFuncionario.getTipoFuncionario(rs.getString("tipo_funcionario")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return funcionario;
	}

}
