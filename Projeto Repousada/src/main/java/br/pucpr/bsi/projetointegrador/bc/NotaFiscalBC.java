package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.bsi.projetointegrador.dao.NotaFiscalDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.NotaFiscal;

public class NotaFiscalBC extends PatternBC<NotaFiscal>{

	private static NotaFiscalBC instance = new NotaFiscalBC();
	
	private NotaFiscalDAO dao = new NotaFiscalDAO();
	
	private NotaFiscalBC() {}
	
	public static NotaFiscalBC getInstance()
	{
		return instance;
	}
	
	@Override
	public Long create(NotaFiscal object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public NotaFiscal retrieve(NotaFiscal object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<NotaFiscal> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(NotaFiscal object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(NotaFiscal object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		dao.delete(object);
	}

	@Override
	protected void validate(NotaFiscal object) 
	{
		if(object == null)
			throw new RepousadaException("Nota fiscal nula!");
		
		else if(StringUtils.isBlank(object.getCnpjPousada()))
			throw new RepousadaException("CNPJ da empresa n�o preenchida!");
		
		else if(object.getReserva() == null)
			throw new RepousadaException("Reserva n�o vinculado � Nota Fiscal!");
		
		else if(object.getPrecoTotal() == null ||  object.getPrecoTotal() <= 0.0f)
			throw new RepousadaException("Valor total n�o calculado!");
		
		ReservaBC.getInstance().validate(object.getReserva());
	}

	@Override
	public List<NotaFiscal> retrievaByFilter(NotaFiscal object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	protected void validateFilter(NotaFiscal object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}

}
