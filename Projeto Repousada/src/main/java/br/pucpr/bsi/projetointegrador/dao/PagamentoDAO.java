package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Pagamento;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.enums.TipoPagamento;

public final class PagamentoDAO implements DataAccessObject<Pagamento> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.pagamento";
	String retrieve = "SELECT id, valor, dt_pagamento, dt_prazo, tp_pagamento, nr_parcelas_restantes, reserva_id"
			+ " FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(Pagamento element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable 
				+ " (valor, dt_pagamento, dt_prazo, tp_pagamento, nr_parcelas_restantes, reserva_id) "
				+ "values (?, ?, ?, ?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
				
				pstm.setDouble(1, element.getValor());
				pstm.setDate(2, new java.sql.Date(element.getDataPagamento().getTime()));
				pstm.setDate(3, new java.sql.Date(element.getDataPrazo().getTime()));
				pstm.setString(4, element.getTipoPagamento().getNome());
				pstm.setLong(5, element.getNrParcelasRestantes());
				pstm.setLong(6, element.getReserva().getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
				try (ResultSet generatedKeys = pstm.getGeneratedKeys()) {
		            if (generatedKeys.next()) {
		            	id = generatedKeys.getLong(1);
		            }  else {
		                throw new RepousadaException("INSERTING ELEMENT FAILED, NO ID OBTAINED.");
		            }
		        }
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(Pagamento element) {
		
		String update = "UPDATE " + dbtable + " SET qtd = ?, data =? WHERE quarto_id = ? AND reserva_id = ? ";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setDouble(1, element.getValor());
				pstm.setDate(2, new java.sql.Date(element.getDataPagamento().getTime()));
				pstm.setDate(3, new java.sql.Date(element.getDataPrazo().getTime()));
				pstm.setString(4, element.getTipoPagamento().getNome());
				pstm.setLong(5, element.getNrParcelasRestantes());
				pstm.setLong(6, element.getReserva().getId());
				pstm.setLong(7, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(Pagamento element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<Pagamento> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<Pagamento> pagamentos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				pagamentos = new ArrayList<>();
				
				while(rs.next()) {
					Pagamento pagamento = new Pagamento();
					pagamento.setId(rs.getLong("id"));
					pagamento.setValor(rs.getDouble("valor"));
					Date dtPag = new Date(rs.getDate("dt_pagamento") == null ? null : rs.getDate("dt_pagamento").getTime());
					pagamento.setDataPagamento(dtPag);
					Date dtPraz = new Date(rs.getDate("dt_prazo") == null ? null : rs.getDate("dt_prazo").getTime());
					pagamento.setDataPagamento(dtPraz);
					String tpPag = rs.getString("tp_pagamento") == null ? null : rs.getString("tp_pagamento");
					pagamento.setTipoPagamento(TipoPagamento.getTipoPagamento(tpPag));
					pagamento.setNrParcelasRestantes(rs.getInt("nr_parcelas_restantes"));
					pagamento.setReserva(new Reserva(rs.getLong("reserva_id")));
					pagamentos.add(pagamento);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return pagamentos;
	}

	@Override
	public Pagamento retrieveById(Pagamento element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getId());
		
		Pagamento pagamento = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					pagamento = new Pagamento();
					pagamento.setId(rs.getLong("id"));
					pagamento.setValor(rs.getDouble("valor"));
					Date dtPag = new Date(rs.getDate("dt_pagamento") == null ? null : rs.getDate("dt_pagamento").getTime());
					pagamento.setDataPagamento(dtPag);
					Date dtPraz = new Date(rs.getDate("dt_prazo") == null ? null : rs.getDate("dt_prazo").getTime());
					pagamento.setDataPagamento(dtPraz);
					String tpPag = rs.getString("tp_pagamento") == null ? null : rs.getString("tp_pagamento");
					pagamento.setTipoPagamento(TipoPagamento.getTipoPagamento(tpPag));
					pagamento.setNrParcelasRestantes(rs.getInt("nr_parcelas_restantes"));
					pagamento.setReserva(new Reserva(rs.getLong("reserva_id")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return pagamento;
	}

	@Override
	public List<Pagamento> retrieveByFilter(Pagamento element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<Pagamento> pagamentos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getId() != null)
				retrieveByFilter.append(" AND id = ?");
			
			if(element.getValor() != null)
				retrieveByFilter.append(" AND valor = ?");
			
			if(element.getDataPagamento() != null)
				retrieveByFilter.append(" AND dt_pagamento = ?");
			
			if(element.getDataPrazo() != null)
				retrieveByFilter.append(" AND dt_prazo = ?");
			
			if(StringUtils.isBlank(element.getTipoPagamento().getNome()))
				retrieveByFilter.append(" AND tp_pagamento = ?");
			
			if(element.getNrParcelasRestantes() != null)
				retrieveByFilter.append(" AND nr_parcelas_restantes = ?");

			if(element.getReserva() != null && element.getReserva().getId() != null)
				retrieveByFilter.append(" AND reserva_id = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getId() != null)
					pstm.setLong(i++, element.getId());
				
				if(element.getValor() != null)
					pstm.setDouble(i++, element.getValor());
				
				if(element.getDataPagamento() != null)
					pstm.setDate(i++, new java.sql.Date(element.getDataPagamento().getTime()));
				
				if(element.getDataPrazo() != null)
					pstm.setDate(i++, new java.sql.Date(element.getDataPrazo().getTime()));
				
				if(StringUtils.isBlank(element.getTipoPagamento().getNome()))
					pstm.setString(i++, element.getTipoPagamento().getNome());
				
				if(element.getNrParcelasRestantes() != null)
					pstm.setLong(i++, element.getNrParcelasRestantes());

				if(element.getReserva() != null && element.getReserva().getId() != null)
					pstm.setLong(i++, element.getReserva().getId());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				pagamentos = new ArrayList<>();
				
				while(rs.next()) {
					Pagamento pagamento = new Pagamento();
					pagamento.setId(rs.getLong("id"));
					pagamento.setValor(rs.getDouble("valor"));
					Date dtPag = new Date(rs.getDate("dt_pagamento") == null ? null : rs.getDate("dt_pagamento").getTime());
					pagamento.setDataPagamento(dtPag);
					Date dtPraz = new Date(rs.getDate("dt_prazo") == null ? null : rs.getDate("dt_prazo").getTime());
					pagamento.setDataPagamento(dtPraz);
					String tpPag = rs.getString("tp_pagamento") == null ? null : rs.getString("tp_pagamento");
					pagamento.setTipoPagamento(TipoPagamento.getTipoPagamento(tpPag));
					pagamento.setNrParcelasRestantes(rs.getInt("nr_parcelas_restantes"));
					pagamento.setReserva(new Reserva(rs.getLong("reserva_id")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return pagamentos;
		
	}

}
