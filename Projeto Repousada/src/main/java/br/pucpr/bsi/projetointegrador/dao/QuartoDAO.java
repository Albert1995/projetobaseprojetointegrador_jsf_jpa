package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public final class QuartoDAO implements DataAccessObject<Quarto> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.quarto";
	String retrieve = "SELECT numero, tipo, situacao FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(Quarto element) {
		
		String insert = "INSERT INTO " + dbtable + " (numero, tipo, situacao) "
				+ "values (?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert)) {
				
				pstm.setLong(1, element.getNumero());
				pstm.setString(2, element.getModelo().getNome());
				pstm.setString(3, element.getSituacao().getNome());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return element.getNumero();
	}

	@Override
	public void update(Quarto element) {
		
		String update = "UPDATE " + dbtable + " SET tipo = ?, situacao = ? WHERE numero = ?";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setString(1, element.getModelo().getNome());
				pstm.setString(2, element.getSituacao().getNome());
				pstm.setLong(3, element.getNumero());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(Quarto element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE numero = ?";
		logger.debug("DELETING ELEMENT ID " + element.getNumero() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getNumero());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getNumero() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<Quarto> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<Quarto> quartos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				quartos = new ArrayList<>();
				
				while(rs.next()) {
					Quarto quarto = new Quarto(rs.getLong("numero"), TipoQuarto.getTipoQuarto(rs.getString("tipo")));
					quarto.setSituacao(SituacaoQuarto.getSituacaoQuarto(rs.getString("situacao")));
					quartos.add(quarto);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return quartos;
	}

	@Override
	public Quarto retrieveById(Quarto element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getNumero());
		
		Quarto quarto = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE numero = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getNumero());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					quarto = new Quarto(rs.getLong("numero"), TipoQuarto.getTipoQuarto(rs.getString("tipo")));
					quarto.setSituacao(SituacaoQuarto.getSituacaoQuarto(rs.getString("situacao")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return quarto;
	}

	@Override
	public List<Quarto> retrieveByFilter(Quarto element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<Quarto> quartos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			boolean whereInserted = false;
			if(element.getNumero() != null)
				retrieveByFilter.append((!whereInserted ? " WHERE " : " AND ") + " numero = ?");
			
			if(element.getModelo() != null)
				retrieveByFilter.append((!whereInserted ? " WHERE " : " AND ") + " tipo = ?");
			
			if(element.getSituacao() != null)
				retrieveByFilter.append((!whereInserted ? " WHERE " : " AND ") + " situacao = ?");

			try (PreparedStatement pstm = con.prepareStatement(retrieveByFilter.toString())) {
				
				int i = 1;
				
				if(element.getNumero() != null)
					pstm.setLong(i++, element.getNumero());
				
				if(element.getModelo() != null)
					pstm.setString(i++, element.getModelo().getNome());
					
				if(element.getSituacao() != null)
					pstm.setString(i++, element.getSituacao().getNome());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				quartos = new ArrayList<>();
				
				while(rs.next()) {
					Quarto quarto = new Quarto(rs.getLong("numero"), TipoQuarto.getTipoQuarto(rs.getString("tipo")));
					quarto.setSituacao(SituacaoQuarto.getSituacaoQuarto(rs.getString("situacao")));
					quartos.add(quarto);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return quartos;
		
	}

}
