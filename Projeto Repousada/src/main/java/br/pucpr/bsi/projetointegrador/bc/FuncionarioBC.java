package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.bsi.projetointegrador.dao.FuncionarioDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Funcionario;

public class FuncionarioBC extends PessoaBC<Funcionario>{
	private static FuncionarioBC instance = new FuncionarioBC();
	
	private FuncionarioDAO dao = new FuncionarioDAO();
	
	private FuncionarioBC() {}
	
	public static FuncionarioBC getInstance()
	{
		return instance;
	}

	@Override
	public Long create(Funcionario object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Funcionario retrieve(Funcionario object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Funcionario> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Funcionario object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(Funcionario object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		dao.delete(object);
	}
	
	public Funcionario autenticar(String matricula, String senha) {
		if(StringUtils.isBlank(matricula))
			throw new RepousadaException("Matr�cula n�o informada!");
		
		else if(StringUtils.isBlank(senha))
			throw new RepousadaException("Senha n�o informada!");
		
		return dao.autenticar(matricula, senha);
	}
	
	protected void validate(Funcionario object)
	{
		super.validate(object);

		if(object.getTipoFuncionario() == null)
			throw new RepousadaException("Tipo de Funcion�rio n�o informado!");
		
		else if(object.getSalario() == null || object.getSalario() <= 0)
			throw new RepousadaException("Sal�rio n�o informado!");
		
		else if(object.getMatricula() == null)
			throw new RepousadaException("Matr�cula n�o informada!");
		
		else if(StringUtils.isBlank(object.getSenha()))
			throw new RepousadaException("Senha n�o informada!");
	}

	@Override
	public List<Funcionario> retrievaByFilter(Funcionario object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	protected void validateFilter(Funcionario object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}
}
