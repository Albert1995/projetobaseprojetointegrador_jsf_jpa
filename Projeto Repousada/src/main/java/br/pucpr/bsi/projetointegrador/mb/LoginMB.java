package br.pucpr.bsi.projetointegrador.mb;


import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

import br.pucpr.bsi.projetointegrador.bc.FuncionarioBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.mb.messages.MessagesUtils;
import br.pucpr.bsi.projetointegrador.mb.utils.ViewUtil;
import br.pucpr.bsi.projetointegrador.models.Funcionario;

@ManagedBean
@SessionScoped
public class LoginMB implements Serializable{
	
	/////////////////////////////////////
	// Atributos
	/////////////////////////////////////
	
	private static final long serialVersionUID = 1L;

	private Funcionario funcionario = new Funcionario();
	
	private String matricula;
	private String senha;
	
	private Funcionario filtroPesquisa = new Funcionario();
	
	/////////////////////////////////////
	// Construtores
	/////////////////////////////////////
	
	public LoginMB() {}
	
	
	/////////////////////////////////////
	// Actions
	/////////////////////////////////////
	
	//Action de Salvar Usuario
	//validate login
    public String validateUsernamePassword() {
        funcionario = FuncionarioBC.getInstance().autenticar(matricula, senha);
        
        if (funcionario == null)
            throw new RepousadaException("Senha ou Matricula incorreta");

        HttpSession session = ViewUtil.getSession();
        session.setAttribute("matricula", matricula);
        
        MessagesUtils.addInfo("sucesso", "Opera��o realizada com sucesso.");
        return "index?faces-redirect=true";
    }
 
    //logout event, invalidate session
    public String logout() {
        HttpSession session = ViewUtil.getSession();
        session.invalidate();
        return "login?faces-redirect=true";
    }
	
	//Action para reset do cadastro
	public void limpar() {
		funcionario = new Funcionario();
	}
	
	/////////////////////////////////////
	// Getters and Setters
	/////////////////////////////////////
	
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Funcionario getFiltroPesquisa() {
		return filtroPesquisa;
	}

	public void setFiltroPesquisa(Funcionario filtroPesquisa) {
		this.filtroPesquisa = filtroPesquisa;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}