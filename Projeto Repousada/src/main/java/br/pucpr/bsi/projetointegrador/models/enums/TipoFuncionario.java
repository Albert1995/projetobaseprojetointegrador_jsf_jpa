package br.pucpr.bsi.projetointegrador.models.enums;

import org.apache.commons.lang3.StringUtils;

public enum TipoFuncionario {
	
	GERENTE(1l, "GERENTE"), 
	ATENTENDENTE(2l, "ATENDENTE"), 
	SERVICOS_DOMESTICOS(3l, "SERVI�OS DOM�STICOS"),  
	FINANCEIRO(4l, "FINANCEIRO"),
	ADMINISTRADOR(5L, "ADMINISTRADOR");
	
	private Long id;
	private String nome;
	
	private TipoFuncionario(Long id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public static TipoFuncionario getTipoFuncionario(String nome) {
		if(StringUtils.isBlank(nome))
			return null;
			
		for (TipoFuncionario tipoFuncionario : TipoFuncionario.values()) {
			if(tipoFuncionario.getNome().equals(nome))
				return tipoFuncionario;
		}
		
		return null;
	}
}
