package br.pucpr.bsi.projetointegrador.models;

import java.io.Serializable;

public class NotaFiscal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4877683186995976970L;
	
	private Long id;
	private String cnpjPousada;
	private Reserva reserva;
	private Double precoTotal;

	public NotaFiscal(String cnpjPousada, Reserva reserva) {
		this.cnpjPousada = cnpjPousada;
		this.setPrecoTotal(reserva.calcularReserva());
		this.reserva = reserva;
	}
	
	public NotaFiscal() {}

	public NotaFiscal emitirNotaFiscal() {
		return this;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpjPousada() {
		return cnpjPousada;
	}
	
	public void setCnpjPousada(String cnpjPousada) {
		this.cnpjPousada = cnpjPousada;
	}
	
	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public Double getPrecoTotal() {
		return precoTotal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaFiscal other = (NotaFiscal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setPrecoTotal(Double precoTotal) {
		this.precoTotal = precoTotal;
	}

}
