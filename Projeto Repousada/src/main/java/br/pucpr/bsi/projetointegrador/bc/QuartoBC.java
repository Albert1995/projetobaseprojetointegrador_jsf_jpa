package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import br.pucpr.bsi.projetointegrador.dao.QuartoDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Quarto;

public class QuartoBC extends PatternBC<Quarto> {

	private static final QuartoBC instance = new QuartoBC();
	
	private QuartoDAO dao = new QuartoDAO();
	
	private QuartoBC() { }
	
	public static QuartoBC getInstance() {
		return instance;
	}

	@Override
	public Long create(Quarto object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Quarto retrieve(Quarto object) {
		if(object == null || (object.getNumero() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Quarto> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Quarto object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(Quarto object) {
		if(object == null || object.getNumero() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		dao.delete(object);
	}

	@Override
	protected void validate(Quarto object) {
		if(object == null)
			throw new RepousadaException("Quarto Nulo");
		else if (object.getNumero() <= 0)
			throw new RepousadaException("Numero do quarto invalido");
		else if(object.getModelo() == null)
			throw new RepousadaException("Tipo do quarto invalido");
		else if(object.getSituacao() == null)
			throw new RepousadaException("Situação do quarto inválida");
	}

	@Override
	public List<Quarto> retrievaByFilter(Quarto object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	protected void validateFilter(Quarto object) {
		if(object == null)
			throw new RepousadaException("Filtro está nulo");
	}

}
