package br.pucpr.bsi.projetointegrador.mb;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;

import br.pucpr.bsi.projetointegrador.bc.QuartoBC;
import br.pucpr.bsi.projetointegrador.models.Quarto;

@ManagedBean
@ViewScoped
public class VerificarDisponibilidadeMB implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5519049457077503383L;
	private List<Quarto> quartos;
	private Quarto quartoSelecionado;
	
	public VerificarDisponibilidadeMB(){
	}
	
	@PostConstruct
	private void init(){
		quartos = QuartoBC.getInstance().retrievaAll();
		quartoSelecionado = new Quarto(null);
	}
	
	
	public void onRowSelect(SelectEvent event) {
		FacesMessage msg = new FacesMessage("Quarto "+String.valueOf(((Quarto) event.getObject()).getNumero()), ((Quarto) event.getObject()).getSituacao().getNome());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public List<Quarto> getQuartos() {
		return quartos;
	}

	public void setQuartos(List<Quarto> quartos) {
		this.quartos = quartos;
	}

	public Quarto getQuartoSelecionado() {
		return quartoSelecionado;
	}

	public void setQuartoSelecionado(Quarto quartoSelecionado) {
		this.quartoSelecionado = quartoSelecionado;
	}
	
}
