package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import br.pucpr.bsi.projetointegrador.dao.ReservaDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.ItemProduto;
import br.pucpr.bsi.projetointegrador.models.ItemServico;
import br.pucpr.bsi.projetointegrador.models.NotaFiscal;
import br.pucpr.bsi.projetointegrador.models.Reserva;

public class ReservaBC extends PatternBC<Reserva>{

	private static ReservaBC instance = new ReservaBC();
	
	private ReservaDAO dao = new ReservaDAO();

	private ReservaBC() {}

	public static ReservaBC getInstance() {
		return instance;
	}

	@Override
	public Long create(Reserva object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Reserva retrieve(Reserva object) {
		if(object == null || (object.getId() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Reserva> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Reserva object) {
		validate(object);
		dao.update(object);
	}
	
	public Reserva checkIn(Reserva object) {
		validate(object);
		
		if(object.getHospedes().isEmpty())
			throw new RepousadaException("N�o h� hospedes para o check in.");
		
		dao.checkIn(object);
		
		return object;
	}

	public Reserva retrieveConsumos(Reserva object) {
		
		object = retrieve(object);
		if(object != null) {
			object = dao.retrieveItemProdutosById(object);
			object = dao.retrieveItemServicosById(object);
		}
		return object;
	}
	
	@Override
	public void delete(Reserva object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Reserva nula");
		
		dao.delete(object);
	}

	@Override
	protected void validate(Reserva object)
	{
		if(object == null)
			throw new RepousadaException("Reserva nula!");

		else if(object.getHospedes() == null || object.getHospedes().isEmpty())
			throw new RepousadaException("Reserva sem h�spedes!");

		else if(object.calcularReserva() <= 0.0)
			throw new RepousadaException("Valor total n�o calculado!");
		
		if(object.getInicio() == null)
			throw new RepousadaException("Data de inicio nula");

		for(ItemServico item : object.getItemServicos())
			ServicoBC.getInstance().validate(item.getServico());

		for(ItemProduto i : object.getItemProdutos())
			ProdutoBC.getInstance().validate(i.getProduto());

		for (NotaFiscal notaFiscal : object.getNotaFiscal())
				NotaFiscalBC.getInstance().validate(notaFiscal);
		
		for(Diaria diaria : object.getDiarias())
			DiariaBC.getInstance().validate(diaria);
		
		for(Hospede hospede : object.getHospedes())
			HospedeBC.getInstance().validate(hospede);

	}

	@Override
	public List<Reserva> retrievaByFilter(Reserva object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	protected void validateFilter(Reserva object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}

}
