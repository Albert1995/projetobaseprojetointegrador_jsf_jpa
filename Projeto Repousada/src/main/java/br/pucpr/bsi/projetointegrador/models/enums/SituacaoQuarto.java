package br.pucpr.bsi.projetointegrador.models.enums;

import org.apache.commons.lang3.StringUtils;

public enum SituacaoQuarto {

	DISPONIVEL(1L, "Disponivel"), RESERVADO(2L, "Reservado"), OCUPADO(3L, "Ocupado"), MANUTENCAO(4L, "Manutencao");
	
	private Long id;
	private String nome;
	
	private SituacaoQuarto(Long id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public static SituacaoQuarto getSituacaoQuarto(String nome) {
		if(StringUtils.isBlank(nome))
			return null;
		
		for (SituacaoQuarto situacaoQuarto : SituacaoQuarto.values()) {
			if(situacaoQuarto.getNome().equals(nome))
				return situacaoQuarto;
		}
		
		return null;
	}
}
