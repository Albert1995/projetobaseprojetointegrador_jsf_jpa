package br.pucpr.bsi.projetointegrador.dao.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class ConnectionHandler {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);

	/**
	 * @return {@link Connection}
	 * @throws SQLException
	 */
	public Connection getSource() throws SQLException {

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			logger.error("Where is your MySQL JDBC Driver?", e);
			e.printStackTrace();
			return null;
		}

		Connection connection = null;

		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/repousada", "root", "root");
		} catch (SQLException e) {
			logger.error("Connection Failed!", e);
			return null;
		}

		if (connection == null) {
			logger.error("Failed to make connection!");
			return null;
		}

		return connection;
	}
}
