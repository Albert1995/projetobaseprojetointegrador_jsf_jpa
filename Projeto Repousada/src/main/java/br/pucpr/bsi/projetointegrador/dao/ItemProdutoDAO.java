package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.ItemProduto;
import br.pucpr.bsi.projetointegrador.models.Produto;
import br.pucpr.bsi.projetointegrador.models.Reserva;

public final class ItemProdutoDAO implements DataAccessObject<ItemProduto> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.item_produto";
	String retrieve = "SELECT reserva_id, produto_id, qtd FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(ItemProduto element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable + " (reserva_id, produto_id, qtd) values (?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getProduto().getId());
				pstm.setLong(3, element.getQuantidade());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();

			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(ItemProduto element) {
		
		String update = "UPDATE " + dbtable + " SET qtd = ? WHERE reserva_id = ? AND produto_id = ?";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setLong(1, element.getQuantidade());
				pstm.setLong(2, element.getReserva().getId());
				pstm.setLong(3, element.getProduto().getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(ItemProduto element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE reserva_id = ? AND produto_id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getReserva().getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getProduto().getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getReserva().getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<ItemProduto> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<ItemProduto> servicos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				servicos = new ArrayList<>();
				
				while(rs.next()) {
					ItemProduto itemProduto = new ItemProduto();
					itemProduto.setQuantidade(rs.getLong("qtd"));
					itemProduto.setReserva(new Reserva(rs.getLong("reserva_id")));
					itemProduto.setProduto(new Produto(rs.getLong("produto_id")));
					servicos.add(itemProduto);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return servicos;
	}

	@Override
	public ItemProduto retrieveById(ItemProduto element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getReserva().getId());
		
		ItemProduto itemProduto = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE reserva_id = ? AND produto_id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getProduto().getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					itemProduto = new ItemProduto();
					itemProduto.setQuantidade(rs.getLong("qtd"));
					itemProduto.setReserva(new Reserva(rs.getLong("reserva_id")));
					itemProduto.setProduto(new Produto(rs.getLong("produto_id")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return itemProduto;
	}

	@Override
	public List<ItemProduto> retrieveByFilter(ItemProduto element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<ItemProduto> servicos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getReserva() != null && element.getReserva().getId() != null)
				retrieveByFilter.append(" AND reserva_id = ?");
			
			if(element.getProduto() != null && element.getProduto().getId() != null)
				retrieveByFilter.append(" AND produto_id = ?");
			
			if(element.getQuantidade() != null)
				retrieveByFilter.append(" AND qtd = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getReserva() != null && element.getReserva().getId() != null)
					pstm.setLong(i++, element.getReserva().getId());
				
				if(element.getProduto() != null && element.getProduto().getId() != null)
					pstm.setLong(i++, element.getProduto().getId());
					
				if(element.getQuantidade() != null)
					pstm.setLong(i++, element.getQuantidade());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				servicos = new ArrayList<>();
				
				while(rs.next()) {
					ItemProduto itemProduto = new ItemProduto();
					itemProduto.setQuantidade(rs.getLong("qtd"));
					itemProduto.setReserva(new Reserva(rs.getLong("reserva_id")));
					itemProduto.setProduto(new Produto(rs.getLong("produto_id")));
					servicos.add(itemProduto);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return servicos;
		
	}

}
