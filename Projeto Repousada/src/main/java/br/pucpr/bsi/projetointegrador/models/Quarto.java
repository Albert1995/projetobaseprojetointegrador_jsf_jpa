package br.pucpr.bsi.projetointegrador.models;

import java.io.Serializable;

import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public class Quarto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5850749542420141872L;
	
	private Long numero;
	private TipoQuarto modelo;
	private SituacaoQuarto situacao;

	public Quarto(Long numero) {
		this.numero = numero;
	}
	
	public Quarto(Long numero, TipoQuarto modelo) {
		this.numero = numero;
		this.modelo = modelo;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public TipoQuarto getModelo() {
		return modelo;
	}

	public void setModelo(TipoQuarto modelo) {
		this.modelo = modelo;
	}

	public SituacaoQuarto getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoQuarto situacao) {
		this.situacao = situacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quarto other = (Quarto) obj;
		if (modelo != other.modelo)
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (situacao != other.situacao)
			return false;
		return true;
	}

}
