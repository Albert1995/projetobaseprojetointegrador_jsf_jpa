package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.Reserva;

public final class DiariaDAO implements DataAccessObject<Diaria> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.diaria";
	String retrieve = "SELECT quarto_id, reserva_id, qtd, data FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(Diaria element) {
		
		String insert = "INSERT INTO " + dbtable + " (quarto_id, reserva_id, qtd, data) "
				+ "values (?, ?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert)) {
				
				pstm.setLong(1, element.getQuarto().getNumero());
				pstm.setLong(2, element.getReserva().getId());
				pstm.setLong(3, element.getQuantidade());
				pstm.setDate(4, new Date(element.getData().getTime()));
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return element.getReserva().getId();
	}

	@Override
	public void update(Diaria element) {
		
		String update = "UPDATE " + dbtable + " SET qtd = ?, data =? WHERE quarto_id = ? AND reserva_id = ? ";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setLong(1, element.getQuantidade());
				pstm.setDate(2, new Date(element.getData().getTime()));
				pstm.setLong(3, element.getQuarto().getNumero());
				pstm.setLong(4, element.getReserva().getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(Diaria element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE reserva_id = ? AND quarto_id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getReserva().getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getQuarto().getNumero());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getReserva().getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<Diaria> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<Diaria> diarias = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				diarias = new ArrayList<>();
				
				while(rs.next()) {
					Diaria diaria = new Diaria();
					diaria.setQuantidade(rs.getLong("qtd"));
					diaria.setData(rs.getDate("data"));
					diaria.setQuarto(new Quarto(rs.getLong("quarto_id")));
					diaria.setReserva(new Reserva(rs.getLong("reserva_id")));
					diarias.add(diaria);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return diarias;
	}

	@Override
	public Diaria retrieveById(Diaria element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getReserva().getId());
		
		Diaria diaria = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE reserva_id = ? AND quarto_id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getQuarto().getNumero());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					diaria = new Diaria();
					diaria.setQuantidade(rs.getLong("qtd"));
					diaria.setData(rs.getDate("data"));
					diaria.setQuarto(new Quarto(rs.getLong("quarto_id")));
					diaria.setReserva(new Reserva(rs.getLong("reserva_id")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return diaria;
	}

	@Override
	public List<Diaria> retrieveByFilter(Diaria element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<Diaria> diarias = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getQuantidade() != null)
				retrieveByFilter.append(" AND qtd = ?");
			
			if(element.getData() != null)
				retrieveByFilter.append(" AND data = ?");

			if(element.getQuarto() != null && element.getQuarto().getNumero() != null)
				retrieveByFilter.append(" AND quarto_id = ?");
			
			if(element.getReserva() != null && element.getReserva().getId() != null)
				retrieveByFilter.append(" AND reserva_id = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getQuantidade() != null)
					pstm.setLong(i++, element.getQuantidade());
				
				if(element.getData() != null)
					pstm.setDate(i++, new Date(element.getData().getTime()));
					
				if(element.getQuarto() != null && element.getQuarto().getNumero() != null)
					pstm.setLong(i++, element.getQuarto().getNumero());
				
				if(element.getReserva() != null && element.getReserva().getId() != null)
					pstm.setLong(i++, element.getReserva().getId());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				diarias = new ArrayList<>();
				
				while(rs.next()) {
					Diaria diaria = new Diaria();
					diaria.setQuantidade(rs.getLong("qtd"));
					diaria.setData(rs.getDate("data"));
					diaria.setQuarto(new Quarto(rs.getLong("quarto_id")));
					diaria.setReserva(new Reserva(rs.getLong("reserva_id")));
					diarias.add(diaria);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return diarias;
		
	}

}
