package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import br.pucpr.bsi.projetointegrador.dao.PagamentoDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Pagamento;


public class PagamentoBC extends PatternBC<Pagamento> {
	private static PagamentoBC instance;
	
	private PagamentoDAO dao = new PagamentoDAO();
	
	public static PagamentoBC getInstance() {
		if(instance == null) {
			instance = new PagamentoBC();
		}
		return instance;
	}

	@Override
	protected void validate(Pagamento object) {
		if (object == null)
			throw new RepousadaException("Pagamento nulo");
		else if (object.getReserva() == null)
			throw new RepousadaException("Reserva nula");
		else if (object.getTipoPagamento() == null)
			throw new RepousadaException("Tipo de pagamento nulo");
		else if (object.getValor() == null || object.getValor() <= 0)
			throw new RepousadaException("Valor da despesa inv�lido");
		else if(object.getDataPagamento() == null)
			throw new RepousadaException("Data de pagamento inv�lida!");
		else if(object.getDataPrazo() == null)
			throw new RepousadaException("Data do prazo inv�lida!");
		else if(object.getNrParcelasRestantes() != null && object.getNrParcelasRestantes() < 0)
			throw new RepousadaException("N�mero de parcelas inv�lido.");
		
		ReservaBC.getInstance().validate(object.getReserva());
	}

	@Override
	public Long create(Pagamento object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Pagamento retrieve(Pagamento object) {
		if(object == null || (object.getId() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Pagamento> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Pagamento object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(Pagamento object) {
		if(object == null || object.getId() == null)
			throw new RepousadaException("Filtro est� nulo");
		
		delete(object);
	}

	@Override
	public List<Pagamento> retrievaByFilter(Pagamento object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	protected void validateFilter(Pagamento object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}
}
