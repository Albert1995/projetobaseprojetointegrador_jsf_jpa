package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.NotaFiscal;
import br.pucpr.bsi.projetointegrador.models.Reserva;

public final class NotaFiscalDAO implements DataAccessObject<NotaFiscal> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.notaFiscal";
	String retrieve = "SELECT id, cnpj, reserva_id, total FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(NotaFiscal element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable + " (cnpj, reserva_id, total) "
				+ "values (?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
				
				pstm.setString(1, element.getCnpjPousada());
				pstm.setLong(2, element.getReserva().getId());
				pstm.setDouble(3, element.getPrecoTotal());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
				try (ResultSet generatedKeys = pstm.getGeneratedKeys()) {
		            if (generatedKeys.next()) {
		            	id = generatedKeys.getLong(1);
		            }  else {
		                throw new RepousadaException("INSERTING ELEMENT FAILED, NO ID OBTAINED.");
		            }
		        }
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(NotaFiscal element) {
		
		String update = "UPDATE " + dbtable + " SET qtd = ?, data =? WHERE quarto_id = ? AND reserva_id = ? ";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setString(1, element.getCnpjPousada());
				pstm.setLong(2, element.getReserva().getId());
				pstm.setDouble(3, element.getPrecoTotal());
				pstm.setLong(4, element.getReserva().getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(NotaFiscal element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<NotaFiscal> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<NotaFiscal> notaFiscals = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				notaFiscals = new ArrayList<>();
				
				while(rs.next()) {
					NotaFiscal notaFiscal = new NotaFiscal();
					notaFiscal.setId(rs.getLong("id"));
					notaFiscal.setCnpjPousada(rs.getString("cnpj"));
					notaFiscal.setReserva(new Reserva(rs.getLong("reserva_id")));
					notaFiscal.setPrecoTotal(rs.getDouble("total"));
					notaFiscals.add(notaFiscal);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return notaFiscals;
	}

	@Override
	public NotaFiscal retrieveById(NotaFiscal element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getId());
		
		NotaFiscal notaFiscal = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					notaFiscal = new NotaFiscal();
					notaFiscal.setId(rs.getLong("id"));
					notaFiscal.setCnpjPousada(rs.getString("cnpj"));
					notaFiscal.setReserva(new Reserva(rs.getLong("reserva_id")));
					notaFiscal.setPrecoTotal(rs.getDouble("total"));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return notaFiscal;
	}

	@Override
	public List<NotaFiscal> retrieveByFilter(NotaFiscal element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<NotaFiscal> notaFiscals = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getId() != null)
				retrieveByFilter.append(" AND id = ?");
			
			if(StringUtils.isBlank(element.getCnpjPousada()))
				retrieveByFilter.append(" AND cnpj = ?");

			if(element.getPrecoTotal() != null)
				retrieveByFilter.append(" AND total = ?");
			
			if(element.getReserva() != null && element.getReserva().getId() != null)
				retrieveByFilter.append(" AND reserva_id = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getId() != null)
					pstm.setLong(i++, element.getId());
				
				if(StringUtils.isBlank(element.getCnpjPousada()))
					pstm.setString(i++, element.getCnpjPousada());
					
				if(element.getPrecoTotal() != null)
					pstm.setDouble(i++, element.getPrecoTotal());
				
				if(element.getReserva() != null && element.getReserva().getId() != null)
					pstm.setLong(i++, element.getReserva().getId());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				notaFiscals = new ArrayList<>();
				
				while(rs.next()) {
					NotaFiscal notaFiscal = new NotaFiscal();
					notaFiscal.setId(rs.getLong("id"));
					notaFiscal.setCnpjPousada(rs.getString("cnpj"));
					notaFiscal.setReserva(new Reserva(rs.getLong("reserva_id")));
					notaFiscal.setPrecoTotal(rs.getDouble("total"));
					notaFiscals.add(notaFiscal);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return notaFiscals;
		
	}

}
