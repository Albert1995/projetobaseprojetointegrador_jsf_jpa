package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import br.pucpr.bsi.projetointegrador.dao.DiariaDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Diaria;

public class DiariaBC extends PatternBC<Diaria> {

	private static final DiariaBC instance = new DiariaBC();
	
	private DiariaDAO dao = new DiariaDAO();
	
	private DiariaBC() {}
	
	public static DiariaBC getInstance() {
		return instance;
	}
	
	@Override
	protected void validate(Diaria object) {
		if (object == null)
			throw new RepousadaException("Diaria Nula");
		else if (object.getQuarto() == null)
			throw new RepousadaException("Quarto nulo");
		else if (object.getData() == null)
			throw new RepousadaException("Data invalida");
		else if (object.getQuantidade() == null || object.getQuantidade() < 0)
			throw new RepousadaException("Quantidade n�o informada");
		else if (object.getReserva() == null)
			throw new RepousadaException("Reserva nula");
		
		QuartoBC.getInstance().validate(object.getQuarto());
	}

	@Override
	public Long create(Diaria object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Diaria retrieve(Diaria object) {
		if(object == null || (object.getQuarto().getNumero() == null && object.getReserva().getId() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Diaria> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Diaria object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(Diaria object) {
		if(object == null || (object.getQuarto().getNumero() == null && object.getReserva().getId() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		dao.delete(object);
	}

	@Override
	public List<Diaria> retrievaByFilter(Diaria object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	protected void validateFilter(Diaria object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}

}
