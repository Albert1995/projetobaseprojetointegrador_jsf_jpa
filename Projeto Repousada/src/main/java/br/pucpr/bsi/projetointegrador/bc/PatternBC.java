package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

public abstract class PatternBC <E>{

	public abstract Long create(E object);
	
	public abstract E retrieve(E object);
	
	public abstract List<E> retrievaAll();
	
	public abstract List<E> retrievaByFilter(E object);
	
	public abstract void update(E object);
	
	public abstract void delete(E object);
	
	protected abstract void validate(E object);
	
	protected abstract void validateFilter(E object);
	
}
