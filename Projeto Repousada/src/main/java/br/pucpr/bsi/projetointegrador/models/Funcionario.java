package br.pucpr.bsi.projetointegrador.models;

import java.text.ParseException;
import java.util.Date;

import br.pucpr.bsi.projetointegrador.models.enums.TipoFuncionario;

public class Funcionario extends Pessoa {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4397577311424671351L;
	
	private String matricula;
	private String senha;
	private TipoFuncionario tipoFuncionario;
	private Double salario;
	
	public Funcionario() {}
	
	public Funcionario(String nome, Date dataNascimento, String cpf,
			String telefone, TipoFuncionario tipoFuncionario, Double salario) throws ParseException{
		super(nome, dataNascimento, cpf, telefone);
		this.tipoFuncionario = tipoFuncionario;
		this.salario = salario;
	}

	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}

	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		return true;
	}
	
}
