package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.bsi.projetointegrador.dao.ProdutoDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Produto;

public class ProdutoBC extends PatternBC<Produto> {
	
	private static final ProdutoBC instance = new ProdutoBC();
	
	private ProdutoDAO dao = new ProdutoDAO();
	
	public static ProdutoBC getInstance() {
		return instance;
	}

	@Override
	public Long create(Produto object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Produto retrieve(Produto object) {
		if(object == null || (object.getId() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Produto> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Produto object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(Produto object) {
		if(object == null || (object.getId() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		dao.delete(object);
	}

	@Override
	public List<Produto> retrievaByFilter(Produto object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}
	
	@Override
	protected void validate(Produto object) {
		if (object == null)
			throw new RepousadaException("Produto Nulo!");
		else if (StringUtils.isBlank(object.getNome()))
			throw new RepousadaException("Nome do produto invalido!");
		else if (object.getPreco() == null || object.getPreco() <= 0) 
			throw new RepousadaException("Valor do produto invalido!");
	}

	@Override
	protected void validateFilter(Produto object) {
		if(object == null || (object.getId() == null && 
				StringUtils.isBlank(object.getNome()) && object.getPreco() == null)) 
			throw new RepousadaException("Filtro Inv�lido");
	}
}
