package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.ItemProduto;
import br.pucpr.bsi.projetointegrador.models.ItemServico;
import br.pucpr.bsi.projetointegrador.models.Produto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.Servico;

public final class ReservaDAO implements DataAccessObject<Reserva> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.reserva";
	String retrieve = "SELECT id, valor, inicio, fim FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(Reserva element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable + " (valor, inicio, fim) VALUES (?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
				
				pstm.setDouble(1, element.calcularReserva());
				pstm.setDate(2, new Date(element.getInicio().getTime()));
				
				Date fim = element.getFim() == null ? null : new Date(element.getFim().getTime());
				pstm.setDate(3, fim);
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
				try (ResultSet generatedKeys = pstm.getGeneratedKeys()) {
		            if (generatedKeys.next()) {
		            	id = generatedKeys.getLong(1);
		            }  else {
		                throw new RepousadaException("INSERTING ELEMENT FAILED, NO ID OBTAINED.");
		            }
		        }
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(Reserva element) {
		
		String update = "UPDATE " + dbtable + " SET valor = ?, inicio = ?, fim = ? WHERE id = ?";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setDouble(1, element.calcularReserva());
				pstm.setDate(2, new Date(element.getInicio().getTime()));
				Date fim = element.getFim() == null ? null : new Date(element.getFim().getTime());
				pstm.setDate(3, fim);
				pstm.setLong(4, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(Reserva element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<Reserva> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<Reserva> reservas = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				reservas = new ArrayList<>();
				
				while(rs.next()) {
					Reserva reserva = new Reserva();
					reserva.setId(rs.getLong("id"));
					reserva.setValorFinal(rs.getDouble("valor"));
					
					Date inicio = rs.getDate("inicio");
					reserva.setInicio(inicio == null? null : new java.util.Date(inicio.getTime()));

					Date fim = rs.getDate("fim");
					reserva.setFim(fim == null ? null : new java.util.Date(fim.getTime()));
					reservas.add(reserva);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return reservas;
	}

	@Override
	public Reserva retrieveById(Reserva element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getId());
		
		Reserva reserva = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					reserva = new Reserva();
					reserva.setId(rs.getLong("id"));
					reserva.setValorFinal(rs.getDouble("valor"));
					
					Date inicio = rs.getDate("inicio");
					reserva.setInicio(inicio == null? null : new java.util.Date(inicio.getTime()));

					Date fim = rs.getDate("fim");
					reserva.setFim(fim == null ? null : new java.util.Date(fim.getTime()));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return reserva;
	}

	@Override
	public List<Reserva> retrieveByFilter(Reserva element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<Reserva> reservas = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			retrieveByFilter.append(" WHERE 1=1");
			
			if(element.getId() != null)
				retrieveByFilter.append(" AND id = ?");
			
			if(element.getValorFinal() != null)
				retrieveByFilter.append(" AND valor = ?");
			
			if(element.getInicio() != null)
				retrieveByFilter.append(" AND inicio = ?");
			
			if(element.getFim() != null)
				retrieveByFilter.append(" AND fim = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveByFilter.toString())) {
				
				int i = 1;
				
				if(element.getId() != null)
					pstm.setLong(i++, element.getId());
				
				if(element.getValorFinal() != null)
					pstm.setDouble(i++, element.getValorFinal());
					
				if(element.getInicio() != null)
					pstm.setDate(i++, new Date(element.getInicio().getTime()));
				
				if(element.getFim() != null)
					pstm.setDate(i++, new Date(element.getFim().getTime()));
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				reservas = new ArrayList<>();
				
				while(rs.next()) {
					Reserva reserva = new Reserva();
					reserva.setId(rs.getLong("id"));
					reserva.setValorFinal(rs.getDouble("valor"));
					
					Date inicio = rs.getDate("inicio");
					reserva.setInicio(inicio == null? null : new java.util.Date(inicio.getTime()));

					Date fim = rs.getDate("fim");
					reserva.setFim(fim == null ? null : new java.util.Date(fim.getTime()));
					reservas.add(reserva);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return reservas;
		
	}
	
	public Reserva checkIn(Reserva element) {

		String insert = "INSERT INTO hospede_reserva (hospede_id, reserva_id) VALUES (?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert)) {
				
				for (Hospede hospede : element.getHospedes()) {
					
					pstm.setLong(1, element.getId());
					pstm.setLong(2, hospede.getId());
					
					logger.debug("EXECUTING QUERY");
					pstm.executeUpdate();
				}
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return element;
	}
	
	public Reserva retrieveItemServicosById(Reserva element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder();

			retrieveByFilter.append("SELECT ITS.qtd, S.tipo, S.preco, S.id");
			retrieveByFilter.append("	FROM repousada.reserva RE");
			retrieveByFilter.append("	INNER JOIN repousada.item_servico ITS");
			retrieveByFilter.append("	ON ITS.reserva_id = RE.id");
			retrieveByFilter.append("	INNER JOIN repousada.servico S");
			retrieveByFilter.append("	ON S.id = ITS.servico_id");
			retrieveByFilter.append("	WHERE RE.id = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveByFilter.toString())) {
				
				pstm.setLong(1, element.getId());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				while(rs.next()) {
					Servico s = new Servico(rs.getLong("id")); 
					s.setPreco(rs.getDouble("preco"));
					s.setTipo(rs.getString("tipo"));
					ItemServico its = new ItemServico();
					its.setReserva(element);
					its.setServico(s);
					its.setQuantidade(rs.getLong("qtd"));
					element.getItemServicos().add(its);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return element;
		
	}
	
	public Reserva retrieveItemProdutosById(Reserva element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder();

			retrieveByFilter.append("SELECT ITP.qtd, P.nome, P.preco, P.id");
			retrieveByFilter.append("	FROM repousada.reserva RE");
			retrieveByFilter.append("	INNER JOIN repousada.item_produto ITP");
			retrieveByFilter.append("	ON ITP.reserva_id = RE.id");
			retrieveByFilter.append("	INNER JOIN repousada.produto P");
			retrieveByFilter.append("	ON P.id = ITP.produto_id");
			retrieveByFilter.append("	WHERE RE.id = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveByFilter.toString())) {
				
				pstm.setLong(1, element.getId());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				while(rs.next()) {
					Produto p = new Produto(rs.getLong("id")); 
					p.setPreco(rs.getDouble("preco"));
					p.setNome(rs.getString("nome"));
					ItemProduto itp = new ItemProduto();
					itp.setReserva(element);
					itp.setProduto(p);
					itp.setQuantidade(rs.getLong("qtd"));
					element.getItemProdutos().add(itp);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return element;
		
	}

}
