package br.pucpr.bsi.projetointegrador.models.enums;

public enum TipoQuarto {

	SIMPLES(1L, "Simples", 1, 30.0),
	DUPLO(2L, "Duplo", 2, 40.0),
	CASAL(3L, "Casal", 2, 35.0),
	FAMILIA(4L, "Familia", 4, 50.0);
	
	private TipoQuarto(Long id, String nome, Integer capacidade, Double preco){
		this.id = id;
		this.nome = nome;
		this.capacidade = capacidade;
		this.preco = preco;
	}
	
	private Long id;
	private String nome;
	private Integer capacidade;
	private Double preco;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	public static TipoQuarto getTipoQuarto(String nome) {
		for (TipoQuarto tipoQuarto : TipoQuarto.values()) {
			if(tipoQuarto.getNome().equals(nome))
				return tipoQuarto;
		}
		
		return null;
	}
	
}
