package br.pucpr.bsi.projetointegrador.models;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Hospede extends Pessoa {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8167732804094325619L;
	
	private List<Reserva> reservas;
	private String cartaoCredito;
	
	public Hospede() {}

	public Hospede(String nome, Date dataNascimento, String cpf, String telefone) throws ParseException {
		super(nome, dataNascimento, cpf, telefone);
		reservas = new ArrayList<Reserva>();
	}
	
	public List<Reserva> getReserva() {
		if(reservas == null) {
			reservas = new ArrayList<Reserva>();
		}
		return reservas;
	}

	public void setReserva(Reserva reserva) {
		reservas.add(reserva);
	}
	
	public String getCartaoCredito() {
		return cartaoCredito;
	}
	
	public void setCartaoCredito(String cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}
}
