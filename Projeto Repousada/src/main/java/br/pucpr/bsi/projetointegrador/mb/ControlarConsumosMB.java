package br.pucpr.bsi.projetointegrador.mb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.bc.ItemProdutoBC;
import br.pucpr.bsi.projetointegrador.bc.ItemServicoBC;
import br.pucpr.bsi.projetointegrador.bc.ProdutoBC;
import br.pucpr.bsi.projetointegrador.bc.ReservaBC;
import br.pucpr.bsi.projetointegrador.bc.ServicoBC;
import br.pucpr.bsi.projetointegrador.mb.messages.MessagesUtils;
import br.pucpr.bsi.projetointegrador.models.ItemProduto;
import br.pucpr.bsi.projetointegrador.models.ItemServico;
import br.pucpr.bsi.projetointegrador.models.Produto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.Servico;

@ManagedBean
@ViewScoped
public class ControlarConsumosMB implements Serializable{
	
	/////////////////////////////////////
	// Atributos
	/////////////////////////////////////
	
	private static final long serialVersionUID = 1L;
	
	//Utilizado para logs via Log4J
	private static Logger log = Logger.getLogger(ControlarConsumosMB.class);
	
	private Reserva reserva;
	
	private List<Servico> servicos;
	private List<Produto> produtos;
	
	private ItemServico novoItemServico;
	private ItemProduto novoItemProduto;
	
	private Long qtdProdutos = 0l;
	private Long qtdServicos = 0l;
	
	/////////////////////////////////////
	// Construtores
	/////////////////////////////////////
	
	public ControlarConsumosMB() {}
	
	@PostConstruct
	private void init(){
		if(reserva == null) {
			reserva = new Reserva();
		}
		
		novoItemServico = new ItemServico();
		novoItemServico.setQuantidade(0l);
		novoItemServico.setServico(new Servico());
		novoItemProduto = new ItemProduto();
		novoItemProduto.setQuantidade(0l);
		novoItemProduto.setProduto(new Produto());
	}
	
	/////////////////////////////////////
	// Actions
	/////////////////////////////////////
	
	//Action de Pesquisar a partir do filtro
	public void pesquisar() {
		reserva = ReservaBC.getInstance().retrieveConsumos(reserva);
		
		if(reserva == null){
			MessagesUtils.addInfo("informacao", "Nenhuma reserva encontrada");
		} else {
			produtos = ProdutoBC.getInstance().retrievaAll();
			List<Produto> copiaProdutos = new ArrayList<>(produtos);
			for (Produto produto : produtos) {
				for (ItemProduto ip : reserva.getItemProdutos()) {
					if(ip.getProduto().getId() == produto.getId()) {
						copiaProdutos.remove(produto);
					}
				}
			}

			produtos = copiaProdutos;
			
			servicos = ServicoBC.getInstance().retrievaAll();
			List<Servico> copiaServicos = new ArrayList<>(servicos);
			for (Servico servico : servicos) {
				for (ItemServico is : reserva.getItemServicos()) {
					if(is.getServico().getId() == servico.getId()) {
						copiaServicos.remove(servico);
					}
				}
			}
			
			servicos = copiaServicos;
		}
	}
	
	public void adicionarProduto(){
		log.debug("Adicionando produto");
		novoItemProduto.setReserva(reserva);
		ItemProdutoBC.getInstance().create(novoItemProduto);
		pesquisar();
	}
	
	public void adicionarServico(){
		log.debug("Adicionando servico");
		novoItemServico.setReserva(reserva);
		ItemServicoBC.getInstance().create(novoItemServico);
		pesquisar();
	}
	
	/////////////////////////////////////
	// Getters and Setters
	/////////////////////////////////////

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public ItemServico getNovoItemServico() {
		return novoItemServico;
	}

	public void setNovoItemServico(ItemServico novoItemServico) {
		this.novoItemServico = novoItemServico;
	}

	public ItemProduto getNovoItemProduto() {
		return novoItemProduto;
	}

	public void setNovoItemProduto(ItemProduto novoItemProduto) {
		this.novoItemProduto = novoItemProduto;
	}

	public List<Servico> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servico> servicos) {
		this.servicos = servicos;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Long getQtdProdutos() {
		return qtdProdutos;
	}

	public void setQtdProdutos(Long qtdProdutos) {
		this.qtdProdutos = qtdProdutos;
	}

	public Long getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(Long qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

}