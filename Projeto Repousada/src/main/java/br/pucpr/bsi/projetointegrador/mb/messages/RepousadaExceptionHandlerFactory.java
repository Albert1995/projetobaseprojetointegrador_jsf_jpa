package br.pucpr.bsi.projetointegrador.mb.messages;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class RepousadaExceptionHandlerFactory extends ExceptionHandlerFactory {
	private ExceptionHandlerFactory parent;

	// this injection handles jsf
	public RepousadaExceptionHandlerFactory(ExceptionHandlerFactory parent) {
		this.parent = parent;
	}

	@Override
	public ExceptionHandler getExceptionHandler() {

		ExceptionHandler handler = new RepousadaExceptionHandler(
				parent.getExceptionHandler());

		return handler;
	}

}
