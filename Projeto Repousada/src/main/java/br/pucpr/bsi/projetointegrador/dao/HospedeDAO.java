package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Hospede;

public final class HospedeDAO implements DataAccessObject<Hospede> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.hospede";
	String retrieve = "SELECT id, nome, dt_nascimento, cpf, telefone, cartao_credito FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(Hospede element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable + " (nome, dt_nascimento, cpf, telefone, cartao_credito) VALUES (?, ?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
				
				pstm.setString(1, element.getNome());
				pstm.setDate(2, new Date(element.getDataNascimento().getTime()));
				pstm.setString(3, element.getCpf());
				pstm.setString(4, element.getTelefone());
				pstm.setString(5, element.getCartaoCredito());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
				try (ResultSet generatedKeys = pstm.getGeneratedKeys()) {
		            if (generatedKeys.next()) {
		            	id = generatedKeys.getLong(1);
		            }  else {
		                throw new RepousadaException("INSERTING ELEMENT FAILED, NO ID OBTAINED.");
		            }
		        }
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(Hospede element) {
		
		String update = "UPDATE " + dbtable + " SET nome = ?, dt_nascimento = ?, cpf = ?,"
				+ " telefone = ?, cartao_credito = ? WHERE id = ?";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setString(1, element.getNome());
				pstm.setDate(2, new Date(element.getDataNascimento().getTime()));
				pstm.setString(3, element.getCpf());
				pstm.setString(4, element.getTelefone());
				pstm.setString(5, element.getCartaoCredito());
				pstm.setLong(6, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(Hospede element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<Hospede> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<Hospede> hospedes = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				hospedes = new ArrayList<>();
				
				while(rs.next()) {
					Hospede hospede = new Hospede();
					hospede.setId(rs.getLong("id"));
					hospede.setNome(rs.getString("nome"));

					Date dt_nasc = rs.getDate("dt_nascimento");
					hospede.setDataNascimento(dt_nasc == null? null : new java.util.Date(dt_nasc.getTime()));

					hospede.setCpf(rs.getString("cpf"));
					hospede.setTelefone(rs.getString("telefone"));
					hospede.setCartaoCredito(rs.getString("cartao_credito"));
					
					hospedes.add(hospede);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return hospedes;
	}

	@Override
	public Hospede retrieveById(Hospede element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getId());
		
		Hospede hospede = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					hospede = new Hospede();
					hospede.setId(rs.getLong("id"));
					hospede.setNome(rs.getString("nome"));

					Date dt_nasc = rs.getDate("dt_nascimento");
					hospede.setDataNascimento(dt_nasc == null? null : new java.util.Date(dt_nasc.getTime()));

					hospede.setNome(rs.getString("cpf"));
					hospede.setCpf(rs.getString("telefone"));
					hospede.setCartaoCredito(rs.getString("cartao_credito"));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return hospede;
	}

	@Override
	public List<Hospede> retrieveByFilter(Hospede element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<Hospede> hospedes = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getId() != null)
				retrieveByFilter.append(" AND id = ?");
			
			if(StringUtils.isNotBlank(element.getNome()))
				retrieveByFilter.append(" AND nome = ?");
			
			if(element.getDataNascimento() != null)
				retrieveByFilter.append(" AND dt_nascimento = ?");
			
			if(StringUtils.isNotBlank(element.getCpf()))
				retrieveByFilter.append(" AND cpf = ?");
			
			if(StringUtils.isNotBlank(element.getCartaoCredito()))
				retrieveByFilter.append(" AND cartao_credito = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getId() != null)
					pstm.setLong(i++, element.getId());
				
				if(StringUtils.isNotBlank(element.getNome()))
					pstm.setString(i++, element.getNome());
					
				if(element.getDataNascimento() != null)
					pstm.setDate(i++, new Date(element.getDataNascimento().getTime()));
				
				if(StringUtils.isNotBlank(element.getCpf()))
					pstm.setString(i++, element.getCpf());
				
				if(StringUtils.isNotBlank(element.getCartaoCredito()))
					pstm.setString(i++, element.getCartaoCredito());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				hospedes = new ArrayList<>();
				
				while(rs.next()) {
					Hospede hospede = new Hospede();
					hospede.setId(rs.getLong("id"));
					hospede.setNome(rs.getString("nome"));

					Date dt_nasc = rs.getDate("dt_nascimento");
					hospede.setDataNascimento(dt_nasc == null ? null : new java.util.Date(dt_nasc.getTime()));

					hospede.setNome(rs.getString("cpf"));
					hospede.setCpf(rs.getString("telefone"));
					hospede.setCartaoCredito(rs.getString("cartao_credito"));
					hospedes.add(hospede);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return hospedes;
		
	}

}
