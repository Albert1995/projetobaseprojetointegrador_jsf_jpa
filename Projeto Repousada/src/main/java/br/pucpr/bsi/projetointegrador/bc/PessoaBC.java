package br.pucpr.bsi.projetointegrador.bc;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.Pessoa;

public abstract class PessoaBC<E extends Pessoa> extends PatternBC<E>{
	
	protected PessoaBC(){}
	
	
	@SuppressWarnings("rawtypes")
	public static PessoaBC getInstance(Pessoa pessoa)
	{
		if(pessoa instanceof Hospede)
			return HospedeBC.getInstance();
		else
			return FuncionarioBC.getInstance();
	}
	
	protected void validate(E object)
	{
		if(object == null)
			throw new RepousadaException("Pessoa nula");
		
		else if(StringUtils.isBlank(object.getCpf()))
			throw new RepousadaException("CPF n�o preenchido!");
		
		else if(object.getDataNascimento() == null || object.getDataNascimento().equals(new Date()) || object.getDataNascimento().after(new Date()))
			throw new RepousadaException("Data de nascimento inv�lida!");
		
		else if(StringUtils.isBlank(object.getNome()))
			throw new RepousadaException("Nome n�o preenchido!");
		
		else if(StringUtils.isBlank(object.getTelefone()))
			throw new RepousadaException("Telefone n�o preenchido!");
	}

}
