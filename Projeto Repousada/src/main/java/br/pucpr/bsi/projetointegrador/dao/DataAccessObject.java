package br.pucpr.bsi.projetointegrador.dao;

import java.util.List;

public interface DataAccessObject<E> {

	public Long insert(E element);
	
	public void update(E element);
	
	public void delete(E element);
	
	public List<E> retrieveAll();
	
	public E retrieveById(E element);
	
	public List<E> retrieveByFilter(E element);
	
}
