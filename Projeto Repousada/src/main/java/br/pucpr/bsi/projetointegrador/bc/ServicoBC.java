package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.bsi.projetointegrador.dao.ServicoDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Servico;

public class ServicoBC extends PatternBC<Servico> {
	
	private static final ServicoBC instance = new ServicoBC();
	
	private ServicoBC() { }
	
	private ServicoDAO dao = new ServicoDAO();
	
	public static ServicoBC getInstance() {
		return instance;
	}

	@Override
	public Long create(Servico object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public Servico retrieve(Servico object) {
		if(object == null || (object.getId() == null))
			throw new RepousadaException("Filtro est� nulo ou incompleto");
		
		return dao.retrieveById(object);
	}

	@Override
	public List<Servico> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public void update(Servico object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(Servico object) {
		if (object == null || object.getId() == null) 
			throw new RepousadaException("Servico nulo");
		
		dao.delete(object);
	}

	@Override
	public List<Servico> retrievaByFilter(Servico object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}
	
	@Override
	protected void validate(Servico object) {
		if(object == null) 
			throw new RepousadaException("Servico Nulo");
		else if(StringUtils.isBlank(object.getTipo()))
			throw new RepousadaException("Tipo do servico invalido");
		else if (object.getPreco() == null || object.getPreco() <= 0) 
			throw new RepousadaException("Preco do servico invalido");
	}
	
	@Override
	protected void validateFilter(Servico object) {
		if(object == null || (object.getId() == null && 
				StringUtils.isBlank(object.getTipo()) && object.getPreco() == null)) 
			throw new RepousadaException("Filtro Inv�lido");
	}

}
