package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.ItemServico;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.Servico;

public final class ItemServicoDAO implements DataAccessObject<ItemServico> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.item_servico";
	String retrieve = "SELECT reserva_id, servico_id, qtd FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(ItemServico element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable + " (reserva_id, servico_id, qtd) values (?, ?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getServico().getId());
				pstm.setLong(3, element.getQuantidade());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(ItemServico element) {
		
		String update = "UPDATE " + dbtable + " SET qtd = ? WHERE reserva_id = ? AND servico_id = ?";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setLong(1, element.getQuantidade());
				pstm.setLong(2, element.getReserva().getId());
				pstm.setLong(3, element.getServico().getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(ItemServico element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE reserva_id = ? AND servico_id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getReserva().getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getServico().getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getReserva().getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<ItemServico> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<ItemServico> servicos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				servicos = new ArrayList<>();
				
				while(rs.next()) {
					ItemServico itemServico = new ItemServico();
					itemServico.setQuantidade(rs.getLong("qtd"));
					itemServico.setReserva(new Reserva(rs.getLong("reserva_id")));
					itemServico.setServico(new Servico(rs.getLong("servico_id")));
					servicos.add(itemServico);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return servicos;
	}

	@Override
	public ItemServico retrieveById(ItemServico element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getReserva().getId());
		
		ItemServico itemServico = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE reserva_id = ? AND servico_id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getReserva().getId());
				pstm.setLong(2, element.getServico().getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					itemServico = new ItemServico();
					itemServico.setQuantidade(rs.getLong("qtd"));
					itemServico.setReserva(new Reserva(rs.getLong("reserva_id")));
					itemServico.setServico(new Servico(rs.getLong("servico_id")));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return itemServico;
	}

	@Override
	public List<ItemServico> retrieveByFilter(ItemServico element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<ItemServico> servicos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getReserva() != null && element.getReserva().getId() != null)
				retrieveByFilter.append(" AND reserva_id = ?");
			
			if(element.getServico() != null && element.getServico().getId() != null)
				retrieveByFilter.append(" AND servico_id = ?");
			
			if(element.getQuantidade() != null)
				retrieveByFilter.append(" AND qtd = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getReserva() != null && element.getReserva().getId() != null)
					pstm.setLong(i++, element.getReserva().getId());
				
				if(element.getServico() != null && element.getServico().getId() != null)
					pstm.setLong(i++, element.getServico().getId());
					
				if(element.getQuantidade() != null)
					pstm.setLong(i++, element.getQuantidade());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				servicos = new ArrayList<>();
				
				while(rs.next()) {
					ItemServico itemServico = new ItemServico();
					itemServico.setQuantidade(rs.getLong("qtd"));
					itemServico.setReserva(new Reserva(rs.getLong("reserva_id")));
					itemServico.setServico(new Servico(rs.getLong("servico_id")));
					servicos.add(itemServico);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return servicos;
		
	}

}
