package br.pucpr.bsi.projetointegrador.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.pucpr.bsi.projetointegrador.dao.util.ConnectionHandler;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Produto;

public final class ProdutoDAO implements DataAccessObject<Produto> {

	private final static Logger logger = Logger.getLogger(ConnectionHandler.class);
	
	String dbtable = "repousada.produto";
	String retrieve = "SELECT id, nome, preco FROM " + dbtable;
	
	ConnectionHandler connectionHandler = new ConnectionHandler();
	
	@Override
	public Long insert(Produto element) {
		
		Long id = null;
		
		String insert = "INSERT INTO " + dbtable + " (nome, preco) values (?, ?)";
		logger.debug("INSERTING NEW ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
				
				pstm.setString(1, element.getNome());
				pstm.setDouble(2, element.getPreco());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
				try (ResultSet generatedKeys = pstm.getGeneratedKeys()) {
		            if (generatedKeys.next()) {
		            	id = generatedKeys.getLong(1);
		            }  else {
		                throw new RepousadaException("INSERTING ELEMENT FAILED, NO ID OBTAINED.");
		            }
		        }
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("NEW ELEMENT: " + element.getClass() + " INSERTED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON INSERTING NEW ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return id;
	}

	@Override
	public void update(Produto element) {
		
		String update = "UPDATE " + dbtable + " SET nome = ?, preco = ? WHERE id = ?";
		logger.debug("UPDATING ELEMENT: " + element.getClass());
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(update)) {
				
				pstm.setString(1, element.getNome());
				pstm.setDouble(2, element.getPreco());
				pstm.setLong(3, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT: " + element.getClass() + " UPDATED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON UPDATING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public void delete(Produto element) {
		
		String delete = "DELETE FROM " + dbtable + " WHERE id = ?";
		logger.debug("DELETING ELEMENT ID " + element.getId() + " FROM " + dbtable);
		
		try (Connection con = connectionHandler.getSource()) {
			
			con.setAutoCommit(false);
			
			try (PreparedStatement pstm = con.prepareStatement(delete)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				pstm.executeUpdate();
				
			} catch (SQLException e) {
				con.rollback();
				throw new RepousadaException(e);
			} finally {
				con.commit();
				logger.info("ELEMENT ID " + element.getId() + " FROM " + dbtable + " DELETED SUCCESSFULLY");
			}
			
			con.setAutoCommit(true);
			
		} catch (SQLException e) {
			logger.error("ERROR ON DELETING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
	}

	@Override
	public List<Produto> retrieveAll() {
		
		logger.debug("RETRIEVING ALL FROM " + dbtable);
		
		List<Produto> produtos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				produtos = new ArrayList<>();
				
				while(rs.next()) {
					Produto produto = new Produto();
					produto.setId(rs.getLong("id"));
					produto.setNome(rs.getString("nome"));
					produto.setPreco(rs.getDouble("preco"));
					produtos.add(produto);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return produtos;
	}

	@Override
	public Produto retrieveById(Produto element) {
		
		logger.debug("RETRIEVING FROM " + dbtable + " BY ID: " + element.getId());
		
		Produto produto = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			String retrieveById = retrieve + " WHERE id = ?";
			
			try (PreparedStatement pstm = con.prepareStatement(retrieveById)) {
				
				pstm.setLong(1, element.getId());
				
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				if(rs.next()) {
					produto = new Produto();
					produto.setId(rs.getLong("id"));
					produto.setNome(rs.getString("nome"));
					produto.setPreco(rs.getDouble("preco"));
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return produto;
	}

	@Override
	public List<Produto> retrieveByFilter(Produto element) {

		logger.debug("RETRIEVING FROM " + dbtable + " WITH FILTERS");
		
		List<Produto> produtos = null;
		
		try (Connection con = connectionHandler.getSource()) {
			
			StringBuilder retrieveByFilter = new StringBuilder(retrieve);
			
			if(element.getId() != null)
				retrieveByFilter.append(" AND id = ?");
			
			if(element.getPreco() != null)
				retrieveByFilter.append(" AND preco = ?");
			
			if(StringUtils.isNotBlank(element.getNome()))
				retrieveByFilter.append(" AND nome = ?");
			
			try (PreparedStatement pstm = con.prepareStatement(retrieve)) {
				
				int i = 1;
				
				if(element.getId() != null)
					pstm.setLong(i++, element.getId());
				
				if(element.getPreco() != null)
					pstm.setDouble(i++, element.getPreco());
					
				if(StringUtils.isNotBlank(element.getNome()))
					pstm.setString(i++, element.getNome());
					
				logger.debug("EXECUTING QUERY");
				ResultSet rs = pstm.executeQuery();
				
				produtos = new ArrayList<>();
				
				while(rs.next()) {
					Produto produto = new Produto();
					produto.setId(rs.getLong("id"));
					produto.setNome(rs.getString("nome"));
					produto.setPreco(rs.getDouble("preco"));
					produtos.add(produto);
				}
				
			} catch (SQLException e) {
				throw new RepousadaException(e);
			}

			logger.info("SELECT ON " + dbtable + "COMPLETED SUCCESSFULLY");
			
		} catch (SQLException e) {
			logger.error("ERROR ON RETRIEVING ELEMENT", e);
			throw new RepousadaException(e);
		}
		
		return produtos;
		
	}

}
