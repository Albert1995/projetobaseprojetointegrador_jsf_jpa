package br.pucpr.bsi.projetointegrador.models;

import java.io.Serializable;
import java.util.Date;

import br.pucpr.bsi.projetointegrador.models.enums.TipoPagamento;

public class Pagamento implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5591571950609232294L;
	
	private Long id;
	private Reserva reserva;
	private Double valor;
	private TipoPagamento tipoPagamento;
	private Date dataPagamento;
	private Date dataPrazo;
	private Integer nrParcelasRestantes;
	
	public Pagamento() {}
	
	public Pagamento(Reserva reserva, Double valor, TipoPagamento tipoPagamento,
			Date dataPagamento, Date dataPrazo, Integer nrParcelas) {
		this.reserva = reserva;
		this.valor = valor;
		this.tipoPagamento = tipoPagamento;
		this.dataPagamento = dataPagamento;
		this.dataPrazo = dataPrazo;
		this.nrParcelasRestantes = nrParcelas;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	
	public Date getDataPrazo() {
		return dataPrazo;
	}

	public void setDataPrazo(Date dataPrazo) {
		this.dataPrazo = dataPrazo;
	}

	public Integer getNrParcelasRestantes() {
		return nrParcelasRestantes;
	}

	public void setNrParcelasRestantes(Integer nrParcelas) {
		this.nrParcelasRestantes = nrParcelas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataPagamento == null) ? 0 : dataPagamento.hashCode());
		result = prime * result + ((dataPrazo == null) ? 0 : dataPrazo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nrParcelasRestantes == null) ? 0 : nrParcelasRestantes.hashCode());
		result = prime * result + ((reserva == null) ? 0 : reserva.hashCode());
		result = prime * result + ((tipoPagamento == null) ? 0 : tipoPagamento.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pagamento other = (Pagamento) obj;
		if (dataPagamento == null) {
			if (other.dataPagamento != null)
				return false;
		} else if (!dataPagamento.equals(other.dataPagamento))
			return false;
		if (dataPrazo == null) {
			if (other.dataPrazo != null)
				return false;
		} else if (!dataPrazo.equals(other.dataPrazo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nrParcelasRestantes == null) {
			if (other.nrParcelasRestantes != null)
				return false;
		} else if (!nrParcelasRestantes.equals(other.nrParcelasRestantes))
			return false;
		if (reserva == null) {
			if (other.reserva != null)
				return false;
		} else if (!reserva.equals(other.reserva))
			return false;
		if (tipoPagamento != other.tipoPagamento)
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}
	
}
