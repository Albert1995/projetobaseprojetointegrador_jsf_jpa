package br.pucpr.bsi.projetointegrador.bc;

import java.util.List;

import br.pucpr.bsi.projetointegrador.dao.ItemServicoDAO;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.ItemServico;

public class ItemServicoBC extends PatternBC<ItemServico>{

	private static final ItemServicoBC instance = new ItemServicoBC();
	
	private ItemServicoDAO dao = new ItemServicoDAO();
	
	private ItemServicoBC() {}
	
	public static ItemServicoBC getInstance() {
		return instance;
	}
	
	@Override
	public Long create(ItemServico object) {
		validate(object);
		return dao.insert(object);
	}

	@Override
	public ItemServico retrieve(ItemServico object) {
		return dao.retrieveById(object);
	}

	@Override
	public List<ItemServico> retrievaAll() {
		return dao.retrieveAll();
	}

	@Override
	public List<ItemServico> retrievaByFilter(ItemServico object) {
		validateFilter(object);
		return dao.retrieveByFilter(object);
	}

	@Override
	public void update(ItemServico object) {
		validate(object);
		dao.update(object);
	}

	@Override
	public void delete(ItemServico object) {
		dao.delete(object);
	}

	@Override
	protected void validate(ItemServico object) {
		if(object == null)
			throw new RepousadaException("Item de Servi�o nulo");
		else if (object.getReserva() == null || object.getReserva().getId() == null)
			throw new RepousadaException("A solicita��o de servi�o deve estar vinculada a uma reserva");
		else if (object.getQuantidade() == null || object.getQuantidade() <= 0)
			throw new RepousadaException("Quantidade de item servi�o n�o informada ou menor ou igual a zero");
		else if (object.getServico() == null || object.getServico().getId() == null)
			throw new RepousadaException("Servi�o n�o informado");
	}

	@Override
	protected void validateFilter(ItemServico object) {
		if(object == null)
			throw new RepousadaException("Filtro est� nulo");
	}

}
