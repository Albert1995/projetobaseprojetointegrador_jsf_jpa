package br.pucpr.bsi.projetointegrador.exceptions;

public class RepousadaException extends RuntimeException{

	private static final long serialVersionUID = 7485070027989416441L;

	public RepousadaException(String message) {
		super(message);
	}
	
	public RepousadaException(Throwable t) {
		super(t);
	}
	
}
