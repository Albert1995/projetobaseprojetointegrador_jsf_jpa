package br.pucpr.bsi.projetointegrador.testes;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.QuartoBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public class TesteQuarto {

	private Quarto q;
	
	@Before
	public void criarQuarto() {
		q = new Quarto(1L, TipoQuarto.DUPLO);
		q.setSituacao(SituacaoQuarto.MANUTENCAO);
	}
	
	@Test
	public void validarQuarto() {
		QuartoBC.getInstance().create(q);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarQuartoNulo() {
		QuartoBC.getInstance().create(null);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarQuartoSemNumero() {
		q.setNumero(0l);
		QuartoBC.getInstance().create(q);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarQuartoSemModelo() {
		q.setModelo(null);
		QuartoBC.getInstance().create(q);
	}
	
}
