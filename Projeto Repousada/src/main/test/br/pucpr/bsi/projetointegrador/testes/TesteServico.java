package br.pucpr.bsi.projetointegrador.testes;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.ServicoBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Servico;

public class TesteServico {
	
	private Servico servico;
	
	@Before
	public void criarServico() {
		servico = new Servico("ServicoA", 123.9);
	}
	
	@Test
	public void validarServicoCompleto() {
		ServicoBC.getInstance().create(servico);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarServicoNulo() {
		ServicoBC.getInstance().create(null);
	}
	

	@Test(expected = RepousadaException.class)
	public void validarServicoNomeVazio() {
		servico.setTipo("                 ");
		ServicoBC.getInstance().create(servico);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarServicoSemNome() {
		servico.setTipo(null);
		ServicoBC.getInstance().create(servico);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarServicoSemPreco() {
		servico.setPreco(0.0);
		ServicoBC.getInstance().create(servico);
	}
	
}
