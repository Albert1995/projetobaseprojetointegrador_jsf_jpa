package br.pucpr.bsi.projetointegrador.testes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.HospedeBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Hospede;

public class TesteHospede 
{
	
	protected Hospede hospede;
	
	@Before
	public void criarHospede()
	{
		try {
			hospede = new Hospede("Karina", new SimpleDateFormat("dd/MM/yyyy").parse("20/07/1985"), "212.418.232-35", "(41)9657-4369");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void validarHospedeCompleto()
	{
		HospedeBC.getInstance().create(hospede);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarHospedeNulo()
	{
		hospede = null;
		HospedeBC.getInstance().create(hospede);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarHospedeComEspacosEmBranco()
	{
		hospede.setCpf("                ");
		hospede.setNome("               ");
		hospede.setTelefone("              ");
		HospedeBC.getInstance().create(hospede);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarHospedeSemNome()
	{
		hospede.setNome(null);
		HospedeBC.getInstance().create(hospede);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarHospedeSemCPF()
	{
		hospede.setCpf(null);
		HospedeBC.getInstance().create(hospede);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarHospedeComDataNascimentoNula()
	{
		hospede.setDataNascimento(null);
		HospedeBC.getInstance().create(hospede);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarHospedeComDataNacimentoIgualDataAtual()
	{
		hospede.setDataNascimento(new Date());
		HospedeBC.getInstance().create(hospede);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarHospedeComDataNascimentoSuperiorDataAtual()
	{
		Date data = null;
		try {
			data = new SimpleDateFormat("dd/MM/yyyy").parse("22/10/2019");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		hospede.setDataNascimento(data);
		HospedeBC.getInstance().create(hospede);
	}
	
}
