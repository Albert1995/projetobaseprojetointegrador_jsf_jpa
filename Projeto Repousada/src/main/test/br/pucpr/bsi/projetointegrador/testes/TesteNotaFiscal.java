package br.pucpr.bsi.projetointegrador.testes;

import java.text.ParseException;
import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.NotaFiscalBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.NotaFiscal;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public class TesteNotaFiscal 
{
	private NotaFiscal notaFiscal;
	private static Reserva reserva;
	private static Diaria diaria;
	private static Quarto quarto;
	
	
	@BeforeClass
	public static void criarReserva()
	{
		try {
			reserva = new Reserva(new Hospede("Lucas", new Date(), "093.730.443-21", "(41)3245-6700"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		quarto = new Quarto(14L, TipoQuarto.SIMPLES);
		quarto.setSituacao(SituacaoQuarto.DISPONIVEL);
	
		diaria = new Diaria(quarto, new Date(), 2L, reserva);
	}
	
	@Before
	public void criarNotaFiscal()
	{
		notaFiscal = new NotaFiscal("01234567890123", reserva);
	}
	
	@Test
	public void validarNotaFiscalCompleta()
	{
		NotaFiscalBC.getInstance().create(notaFiscal);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarNotaFiscalSemReserva()
	{
		notaFiscal.setReserva(null);
		NotaFiscalBC.getInstance().create(notaFiscal);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarNotaFiscalSemEmpresa()
	{
		notaFiscal.setCnpjPousada(null);
		NotaFiscalBC.getInstance().create(notaFiscal);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarNotaFiscalEmpresaComEspacosEmBranco()
	{
		notaFiscal.setCnpjPousada("               ");
		NotaFiscalBC.getInstance().create(notaFiscal);
	}
	
	//TODO: H� necessidade de criar um teste para o m�todo  emitirNotaFiscal()??
	@Test
	public void validarEmissarNotaFiscal(){
	}
}
