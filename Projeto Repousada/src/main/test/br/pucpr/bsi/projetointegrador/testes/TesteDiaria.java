package br.pucpr.bsi.projetointegrador.testes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.DiariaBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public class TesteDiaria {
	
	private Diaria d;
	private static Reserva r;
	private static Quarto q;
	
	
	@BeforeClass
	public static void criarReserva(){

		q = new Quarto(1L, TipoQuarto.SIMPLES);
		q.setSituacao(SituacaoQuarto.DISPONIVEL);
		
		try {
			r = new Reserva(new Hospede("Maria", new SimpleDateFormat("yyyy-MM-dd").parse("1987-02-21"), "075.321.647-13", "(41)9457-3251"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void criarDiaria() {
		d = new Diaria(q, new Date(), 1L, r);
	}
	
	@Test
	public void validarDiariaCompleta() {
		DiariaBC.getInstance().create(d);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarDiariaNula() {
		DiariaBC.getInstance().create(null);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarDiariaSemQuarto() {
		d.setQuarto(null);
		DiariaBC.getInstance().create(d);
	}

	@Test(expected = RepousadaException.class)
	public void validarDiariaSemData() {
		d.setData(null);
		DiariaBC.getInstance().create(d);
	}
	
}
