package br.pucpr.bsi.projetointegrador.testes;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.ProdutoBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Produto;

public class TesteProduto {
	
	private Produto p;
	
	@Before
	public void criarProduto() {
		p = new Produto();
		p.setNome("ProdutoA");
		p.setPreco(123.123);
	}
	
	@Test
	public void validarProdutoCompleto() {
		ProdutoBC.getInstance().create(p);
	}

	@Test(expected = RepousadaException.class)
	public void validarProdutoNulo() {
		ProdutoBC.getInstance().create(null);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarProdutoNomeVazio() {
		p.setNome("                ");
		ProdutoBC.getInstance().create(p);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarProdutoSemNome() {
		p.setNome(null);
		ProdutoBC.getInstance().create(p);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarProdutoSemValor() {
		p.setPreco(0.0);
		ProdutoBC.getInstance().create(p);
	}
	
}
