package br.pucpr.bsi.projetointegrador.testes;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.BeforeClass;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.ReservaBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.NotaFiscal;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public class TesteReserva 
{
	protected static Reserva reserva;
	protected NotaFiscal notaFiscal;
	protected static Hospede hospede;
	protected static Diaria diaria1;
	protected static Diaria diaria2;
	protected static Quarto quarto1;
	protected static Quarto quarto2;
	
	@BeforeClass
	public static void criarReserva(){
		try {
			hospede = new Hospede("Karina", new SimpleDateFormat("dd/MM/yyyy").parse("20/07/1985"), "212.418.232-35", "(41)9657-4369");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		reserva = new Reserva(hospede);


		quarto1 = new Quarto(3l, TipoQuarto.CASAL);
		quarto1.setSituacao(SituacaoQuarto.DISPONIVEL);
		
		try {
			diaria1 = new Diaria(quarto1, new SimpleDateFormat("dd/MM/yyyy").parse("22/05/2015"), 1L, reserva);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		quarto2 = new Quarto(10L, TipoQuarto.DUPLO);
		quarto2.setSituacao(SituacaoQuarto.DISPONIVEL);
		try {
			diaria2 = new Diaria(quarto2, new SimpleDateFormat("dd/MM/yyyy").parse("23/05/2015"), 3L, reserva);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
	}
	
	@Test
	public void validarReservaCompleta()
	{
		ReservaBC.getInstance().create(reserva);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarReservaNula()
	{
		reserva = null;
		ReservaBC.getInstance().create(reserva);
	}
	
}
