package br.pucpr.bsi.projetointegrador.testes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Funcionario;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.ItemServico;
import br.pucpr.bsi.projetointegrador.models.NotaFiscal;
import br.pucpr.bsi.projetointegrador.models.Pagamento;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.Servico;
import br.pucpr.bsi.projetointegrador.models.enums.TipoFuncionario;
import br.pucpr.bsi.projetointegrador.models.enums.TipoPagamento;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public class ConsistenciaModels {

	private Funcionario f ;
	private Hospede h;
	private Reserva r;
	private NotaFiscal nf;
	private Pagamento p;
	private Quarto q;
	private Servico s;
	private ItemServico is;
	private Diaria d;
	
	@Before
	public void init(){
		try {
			f = new Funcionario("Carla", new SimpleDateFormat("yyyy-MM-dd").parse("1982-07-30"), "000", "000", TipoFuncionario.SERVICOS_DOMESTICOS, 1000.0);
			h = new Hospede("Roberto", new SimpleDateFormat("yyyy-MM-dd").parse("1990-05-17"), "000", "000");
			r = new Reserva(h);
			p = new Pagamento(r, 20.00, TipoPagamento.DEBITO, new SimpleDateFormat("yyyy-MM-dd").parse("2015-06-30"), new SimpleDateFormat("yyyy-MM-dd").parse("2015-06-30"), 0);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		q = new Quarto(Long.valueOf(1), TipoQuarto.DUPLO);
		d = new Diaria(q, new Date(), Long.valueOf(3), r);
		nf = new NotaFiscal("01234567890123", r);
		s =  new Servico("Pedido na Copa", 15.00);
		is = new ItemServico(s, 2l, r);
	}
	
	/*
	 * A classes de teste servir�o para testar as funcionalidades do 
	 * projeto. Logo deve-se criar um teste para cada funcionalidade.
	 * Por exemplo, emitir nota fiscal, para emitr uma, precisa criar
	 * uma reserva, e conter alguns elementos, depois manda emitir a nota,
	 * depois confira se a nota gerada � a esperada. Abaixo possui alguns
	 * exemplos simples.
	 * 
	 * Albert Weihermann
	 */

	@Test
	public void criarQuarto() {
		
		//Verifica se o quarto n�o � null
		Assert.assertNotNull(q);
		
		//Verifica se o n�mero do quarto � 1, como definido via constutor
		Assert.assertEquals(Long.valueOf(1), q.getNumero());
		
		//Verifica se o modelo do quarto � Luxo, como definido via construtor
		Assert.assertEquals(TipoQuarto.DUPLO, q.getModelo());
		
		//Verificar se o preco do quarto � R$40,00
		Assert.assertEquals(40.0, q.getModelo().getPreco(), 0.0);
		
		//Verificar se a capacidade do quarto � 3 pessoas
		Assert.assertEquals(Integer.valueOf(2), q.getModelo().getCapacidade());
	}
	
	
	@Test
	public void criarDiaria() {

		//Verifica se a di�ria n�o � nula
		Assert.assertNotNull(d);
		
		//Verifica se o quarto foi adicionado � di�ria
		Assert.assertEquals(q, d.getQuarto());
		
	}
	
	@Test
	public void criarHospede() throws ParseException {
		//Verifica se o h�spede n�o � null
		Assert.assertNotNull(h);
		
		//Verifica se o nome do h�spede � Roberto, como definido via construtor
		Assert.assertEquals("Roberto", h.getNome());
		
		//Verifica se a data de nascimento � 17/05/1990, como definido via construtor
		Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("1990-05-17"), h.getDataNascimento());
		
		//Verifica se o CPF � igual a 000, como definido via construtor
		Assert.assertEquals("000", h.getCpf());
		
		//Verifica se o Telefone � igual a 000, como definido via construtor
		Assert.assertEquals("000", h.getTelefone());
	}
	
	@Test
	public void criarFuncionario() throws ParseException {
		
		//Verifica se o funcion�rio n�o � null
		Assert.assertNotNull(f);
		
		//Verifica se o nome � igual a Carla, como definido via construtor
		Assert.assertEquals("Carla", f.getNome());
		
		//Verifica se a data de nascimento � igual a 30/07/1982, como definido via construtor
		Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("1982-07-30"), f.getDataNascimento());
		
		//Verifica se o CPF � igual a 000, como definido via construtor
		Assert.assertEquals("000", f.getCpf());
		
		//Verifica se o telefone � igual a 000, como definido via construtor
		Assert.assertEquals("000", f.getTelefone());
		
		//Verifica se o setor � igual a Limpeza, como definido via construtor
		Assert.assertEquals(TipoFuncionario.SERVICOS_DOMESTICOS, f.getTipoFuncionario());
	}
	
	@Test
	public void criarServico() {
		Assert.assertNotNull(s);
		Assert.assertEquals("Pedido na Copa", s.getTipo());
		Assert.assertEquals(15.00, s.getPreco(), 0.0);
	}
	
	@Test
	public void criarItemServico() {
		Assert.assertNotNull(is);
		Assert.assertEquals(s, is.getServico());
		Assert.assertEquals(Long.valueOf(2), is.getQuantidade());
	}
	
	@Test
	public void criarReserva(){
		List<Diaria> diarias = new ArrayList<Diaria>();
		diarias.add(d);
		List<Hospede> hospedes = new ArrayList<Hospede>();
		hospedes.add(h);
		List<ItemServico> itemServico = new ArrayList<ItemServico>();
		itemServico.add(is);
		
		Assert.assertNotNull(r);
		
		r.getItemServicos().addAll(itemServico);
		
		for(Diaria diaria : r.getDiarias())
			Assert.assertEquals(diaria, d);
		
		for(Hospede hospede : r.getHospedes())
			Assert.assertEquals(hospede, h);
		
		for(ItemServico item : r.getItemServicos())
			Assert.assertEquals(item, is);
		
	}

	@Test
	public void criarPagamento() throws ParseException {
		Assert.assertNotNull(p);
		Assert.assertEquals(r, p.getReserva());
		Assert.assertEquals(20.00f, p.getValor(), 0.0f);
		Assert.assertEquals(TipoPagamento.DEBITO, p.getTipoPagamento());
		Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2015-06-30"), p.getDataPagamento());
		Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2015-06-30"), p.getDataPrazo());
		Assert.assertEquals(Integer.valueOf(0), p.getNrParcelasRestantes());
	}
	
	@Test
	public void criarNotaFiscal(){
		Assert.assertNotNull(nf);
		Assert.assertEquals("01234567890123", nf.getCnpjPousada());
		Assert.assertTrue(nf.getReserva().getHospedes().contains(h));
		Assert.assertEquals(0.0, nf.getPrecoTotal(), 0.0);
	}
	
}
