package br.pucpr.bsi.projetointegrador.testes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.FuncionarioBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Funcionario;
import br.pucpr.bsi.projetointegrador.models.enums.TipoFuncionario;

public class TesteFuncionario 
{
	protected Funcionario funcionario;
	
	@Before
	public void criarFuncionario()
	{
		try {
			funcionario = new Funcionario("Cristina", new SimpleDateFormat("dd/MM/yyyy").parse("11/08/1970"), "846.306.355-13", "(41)9670-3401", TipoFuncionario.GERENTE, 10000.0);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void validarFuncionarioCompleto()
	{
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioNulo()
	{
		funcionario = null;
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioComEspacosEmBranco()
	{
		funcionario.setCpf("                ");
		funcionario.setNome("               ");
		funcionario.setTelefone("              ");
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioSemNome()
	{
		funcionario.setNome(null);
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioSemCPF()
	{
		funcionario.setCpf(null);
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioComDataNascimentoNula()
	{
		funcionario.setDataNascimento(null);
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioComDataNacimentoIgualDataAtual()
	{
		funcionario.setDataNascimento(new Date());
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioComDataNascimentoSuperiorDataAtual()
	{
		Date data = null;
		try {
			data = new SimpleDateFormat("dd/MM/yyyy").parse("22/10/2019");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		funcionario.setDataNascimento(data);
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioSemTipoFuncionario()
	{
		funcionario.setTipoFuncionario(null);
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioComSalarioNulo()
	{
		funcionario.setSalario(null);
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarFuncionarioComSalarioNegativo()
	{
		funcionario.setSalario(-15.0);
		FuncionarioBC.getInstance().create(funcionario);
	}
}
