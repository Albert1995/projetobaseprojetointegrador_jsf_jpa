package br.pucpr.bsi.projetointegrador.testes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucpr.bsi.projetointegrador.bc.PagamentoBC;
import br.pucpr.bsi.projetointegrador.exceptions.RepousadaException;
import br.pucpr.bsi.projetointegrador.models.Diaria;
import br.pucpr.bsi.projetointegrador.models.Hospede;
import br.pucpr.bsi.projetointegrador.models.Pagamento;
import br.pucpr.bsi.projetointegrador.models.Quarto;
import br.pucpr.bsi.projetointegrador.models.Reserva;
import br.pucpr.bsi.projetointegrador.models.enums.SituacaoQuarto;
import br.pucpr.bsi.projetointegrador.models.enums.TipoPagamento;
import br.pucpr.bsi.projetointegrador.models.enums.TipoQuarto;

public class TestePagamento {
	
	private Pagamento pagamento;
	private static Reserva reserva;
	private static Diaria diaria;
	private static Hospede hospede;
	private static Quarto quarto;
	
	@BeforeClass
	public static void criarDadosNecessario() {
		try {
			hospede = new Hospede("Luisa", new SimpleDateFormat("yyyy-MM-dd").parse("1996-03-23"), "095.567.130.10", "(41)3245-0049");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		reserva = new Reserva(hospede);
		
		quarto = new Quarto(23L, TipoQuarto.CASAL);
		quarto.setSituacao(SituacaoQuarto.OCUPADO);
		
		diaria = new Diaria(quarto, new Date(), 5L, reserva);

		try {
			hospede = new Hospede("Joao", new SimpleDateFormat("yyyy-MM-dd").parse("1991-06-01"), "120.948.109-12", "(12)3412-5552");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		hospede.getReserva().add(reserva);
	}
	
	@Before
	public void criarPagamento() {
		pagamento = new Pagamento(reserva, 290.00, TipoPagamento.DEBITO, new Date(), new Date(), 0);
	}
	
	@Test
	public void validarPagamentoCompleto() {
		PagamentoBC.getInstance().create(pagamento);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarPagamentoNulo() {
		PagamentoBC.getInstance().create(null);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarPagamentoSemReserva() {
		pagamento.setReserva(null);
		PagamentoBC.getInstance().create(pagamento);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarPagamentoSemTipoPagamento() {
		pagamento.setTipoPagamento(null);
		PagamentoBC.getInstance().create(pagamento);
	}

	@Test(expected = RepousadaException.class)
	public void validarPagamentoValorInvalido() {
		pagamento.setValor(-1.0);
		PagamentoBC.getInstance().create(pagamento);
	}

	@Test(expected = RepousadaException.class)
	public void validarPagamentoSemDataPrazo() {
		pagamento.setDataPrazo(null);
		PagamentoBC.getInstance().create(pagamento);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarPagamentoSemDataPagamento() {
		pagamento.setDataPagamento(null);
		PagamentoBC.getInstance().create(pagamento);
	}
	
	@Test(expected = RepousadaException.class)
	public void validarPagamentoComNrParcelasInvalidas() {
		pagamento.setNrParcelasRestantes(-1);
		PagamentoBC.getInstance().create(pagamento);
	}
}
